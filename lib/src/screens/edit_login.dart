import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:supplywell_teacher_app/src/screens/nav.dart';
import 'package:supplywell_teacher_app/src/screens/profile.dart';
import 'package:supplywell_teacher_app/src/services/users.dart';
import 'package:supplywell_teacher_app/src/widgets/app_header.dart';

class EditLoginDetails extends StatefulWidget {
  @override
  _EditLoginDetailsState createState() => _EditLoginDetailsState();
}

class _EditLoginDetailsState extends State<EditLoginDetails> {
  bool isLoading = false;

  /// Convert a color hex-string to a Color object.
  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String _email = '', _password = '';
  TextEditingController _emailController = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getProfileDetails();
  }

  getProfileDetails() async {
    isLoading = true;
    User instance = User();
    var res = await instance.getUserProfileDetails();
    isLoading = false;
    Map data = json.decode(res.body);
    setState(() {
      _email = data['contact']['email'] as String;
    });
    _emailController.text = _email;
  }

  navigateToProfile() {
    Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => Profile()),
    );
  }

  navigateToNotification() {
    Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => Nav(3)),
    );
  }

  void submit(params) async {
    User instance = User();
    var res = await instance.updateUserProfile(params);
    print(res.statusCode);
    if (res.statusCode == 200) {
      navigateToProfile();
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget _getSubHeadingText() {
      return Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(bottom: 30.0),
        child: Text(
          'Edit Login Details',
          style: TextStyle(
            color: getColorFromHex('#0b1a3b'),
            fontWeight: FontWeight.bold,
            fontSize: 20.0,
          ),
        ),
      );
    }

    Widget _getEmailField() {
      return TextField(
          controller: _emailController,
          autocorrect: false,
          decoration: InputDecoration(
            labelText: 'Email address',
            border: OutlineInputBorder(),
          ),
          onChanged: (String nameVal) {
            _email = nameVal;
          });
    }

    Widget _passwordField() {
      return TextField(
          autocorrect: false,
          keyboardType: TextInputType.visiblePassword,
          decoration: InputDecoration(
            labelText: 'Password',
            border: OutlineInputBorder(),
          ),
          onChanged: (String nameVal) {
            _password = nameVal;
          });
    }

    Widget submitBtn() {
      return Container(
        width: MediaQuery.of(context).size.width,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: getColorFromHex('#51dbf2'), // background
            padding: EdgeInsets.all(20.0),
          ),
          child: Text(
            'Submit',
            style: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          onPressed: () {
            if (!_formKey.currentState!.validate()) {
              return;
            }
            _formKey.currentState!.save();

            if (_email == '' && _email == null) {
            } else if (_password == '' && _password == null) {
            } else {
              Map data = {'email': _email, 'password': _password};
              submit(data);
            }

            //Send to API
          },
        ),
      );
    }

    Widget _getFormFields() {
      return Container(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _getEmailField(),
              SizedBox(height: 30.0),
              _passwordField(),
              SizedBox(height: 30.0),
              submitBtn()
            ],
          ),
        ),
      );
    }

    Widget getProfileIcon() {
      return GestureDetector(
        onTap: () {
          navigateToProfile();
        },
        child: Icon(
          Icons.account_circle_sharp,
          size: 30.0,
          color: Colors.white,
        ),
      );
    }

    Widget getTitle() {
      return Text(
        'n.',
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.white,
          fontSize: 30.0,
          fontWeight: FontWeight.bold,
        ),
      );
    }

    Widget getNotificationIcon() {
      return GestureDetector(
        onTap: () {
          navigateToNotification();
        },
        child: Icon(
          Icons.circle_notifications,
          size: 30.0,
          color: Colors.white,
        ),
      );
    }

    PreferredSizeWidget _header() {
      return PreferredSize(
        child: SafeArea(
          child: Container(
            child: Padding(
              padding: EdgeInsets.all(20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      navigateToProfile();
                    },
                    child: Icon(
                      Icons.arrow_left_outlined,
                      size: 30.0,
                      color: Colors.white,
                    ),
                  ),
                  getProfileIcon(),
                  getTitle(),
                  getNotificationIcon()
                ],
              ),
            ),
          ),
        ),
        preferredSize: Size.fromHeight(100),
      );
    }

    Widget _getBody() {
      return Container(
        color: getColorFromHex('#e2e7f0'),
        padding: EdgeInsets.fromLTRB(45.0, 30.0, 30.0, 30.0),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: isLoading
            ? Center(child: CircularProgressIndicator())
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [_getSubHeadingText(), _getFormFields()],
              ),
      );
    }

    return Scaffold(
      backgroundColor: getColorFromHex('0b1a3b'),
      appBar: CustomAppBar(true, true, 1),
      body: _getBody(),
    );
  }
}
