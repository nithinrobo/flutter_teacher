import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:supplywell_teacher_app/src/screens/nav.dart';
import 'package:supplywell_teacher_app/src/screens/profile.dart';
import 'package:supplywell_teacher_app/src/services/users.dart';
import 'dart:convert';
import 'dart:developer';

import 'package:supplywell_teacher_app/src/widgets/app_header.dart';

class EditBio extends StatefulWidget {
  @override
  _EditBioState createState() => _EditBioState();
}

class _EditBioState extends State<EditBio> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String _bio = '';
  TextEditingController _bioCtrl = new TextEditingController();
  bool isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getProfileDetails();
  }

  getProfileDetails() async {
    isLoading = true;
    User instance = User();
    var res = await instance.getUserProfileDetails();
    isLoading = false;
    var data = json.decode(res.body);
    setState(() {
      _bio = data['bio'] as String;
    });
    _bioCtrl.text = _bio;
  }

  submit(params) async {
    User instance = User();
    var res = await instance.updateUserProfile(params);
    var data = json.decode(res.body);
    if (res.statusCode == 200) {
      navigateToProfile();
    }
  }

  navigateToProfile() {
    Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => Profile()),
    );
  }

  navigateToNotification() {
    Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => Nav(3)),
    );
  }

  showFlutterToaster(msg) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.TOP,
      timeInSecForIosWeb: 3,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  @override
  Widget build(BuildContext context) {
    /// Convert a color hex-string to a Color object.
    Color getColorFromHex(String hexColor) {
      hexColor = hexColor.toUpperCase().replaceAll('#', '');

      if (hexColor.length == 6) {
        hexColor = 'FF' + hexColor;
      }

      return Color(int.parse(hexColor, radix: 16));
    }

    Widget _getSubHeadingText() {
      return Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(bottom: 30.0),
        child: Text(
          'Edit Bio',
          style: TextStyle(
            color: getColorFromHex('#0b1a3b'),
            fontWeight: FontWeight.bold,
            fontSize: 20.0,
          ),
        ),
      );
    }

    Widget _getBioFormField() {
      return TextField(
          controller: _bioCtrl,
          maxLines: 4,
          autocorrect: false,
          decoration: InputDecoration(
            labelText: 'Edit Bio',
            border: OutlineInputBorder(),
          ),
          onChanged: (String nameVal) {
            _bio = nameVal;
          });
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: getColorFromHex('e2e7f0'),
      appBar: CustomAppBar(true, true, 1),
      body: Container(
        padding: EdgeInsets.fromLTRB(45.0, 30.0, 30.0, 30.0),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: getColorFromHex('e2e7f0'),
        child: isLoading
            ? Center(child: CircularProgressIndicator())
            : Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    _getSubHeadingText(),
                    _getBioFormField(),
                    SizedBox(height: 30.0),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: RaisedButton(
                        padding: EdgeInsets.all(20.0),
                        color: getColorFromHex('#51dbf2'),
                        child: Text(
                          'Submit',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        onPressed: () {
                          //Send to API
                          if (_bio == '' || _bio == null) {
                            showFlutterToaster('Please enter bio');
                          } else {
                            Map params = {'bio': _bio};
                            submit(params);
                          }
                        },
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }
}
