import 'package:flutter/material.dart';
import 'package:supplywell_teacher_app/src/services/booking.dart';
import 'dart:convert';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:supplywell_teacher_app/src/widgets/app_header.dart';

class GoogleMapView extends StatefulWidget {
  String publicId;

  GoogleMapView(this.publicId);

  @override
  _GoogleMapState createState() => _GoogleMapState();
}

class _GoogleMapState extends State<GoogleMapView> {
  var _schoolDetails = {};
  Set<Marker> _markers = {};
  bool isLoading = false;

  @override
  void initState() {
    getSchool();
    super.initState();
  }

  _onMapCreated(GoogleMapController controller) {
    setState(() {
      _markers.add(
        Marker(
          markerId: MarkerId('id-1'),
          position: LatLng(
            double.parse(_schoolDetails['latitude']),
            double.parse(_schoolDetails['longitude']),
          ),
          infoWindow: InfoWindow(
              title: _schoolDetails['name'], snippet: _schoolDetails['city']),
        ),
      );
    });
  }

  getSchool() async {
    Bookings instance = Bookings();
    isLoading = true;
    var res = await instance.getSchool({'publicId': widget.publicId});
    isLoading = false;
    if (res.statusCode == 200) {
      setState(() {
        _schoolDetails = json.decode(res.body);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: CustomAppBar(false, true, 1),
      backgroundColor: getColorFromHex('#e2e7f0'),
      body: isLoading
          ? Center(child: CircularProgressIndicator())
          : GoogleMap(
              onMapCreated: _onMapCreated,
              markers: _markers,
              initialCameraPosition: CameraPosition(
                target: LatLng(
                  double.parse(_schoolDetails['latitude']),
                  double.parse(_schoolDetails['longitude']),
                ),
                zoom: 11.5,
              ),
            ),
    );
  }

  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  Widget getProfileIcon() {
    return GestureDetector(
      onTap: () {},
      child: Icon(
        Icons.account_circle_sharp,
        size: 30.0,
        color: Colors.white,
      ),
    );
  }

  Widget getTitle() {
    return Text(
      'n.',
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Colors.white,
        fontSize: 30.0,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  Widget getNotificationIcon() {
    return GestureDetector(
      onTap: () {},
      child: Icon(
        Icons.circle_notifications,
        size: 30.0,
        color: Colors.white,
      ),
    );
  }
}
