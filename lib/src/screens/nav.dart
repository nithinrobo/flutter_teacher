import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:supplywell_teacher_app/src/screens/booking.dart';
import 'package:supplywell_teacher_app/src/screens/booking_list.dart';
import 'package:supplywell_teacher_app/src/screens/calendar.dart';
import 'package:supplywell_teacher_app/src/screens/dashboard.dart';
import 'package:supplywell_teacher_app/src/screens/notifications.dart';
import 'package:supplywell_teacher_app/src/screens/profile.dart';
import 'package:supplywell_teacher_app/src/screens/timesheets.dart';
import 'package:supplywell_teacher_app/src/services/users.dart';
import 'package:supplywell_teacher_app/src/widgets/app_header.dart';

class Nav extends StatefulWidget {
  int index;

  Nav(this.index);

  @override
  _NavState createState() => _NavState();
}

class _NavState extends State<Nav> {
  int _selectedIndex = 0;
  var _dashboard = {};
  List _todaysBook = [];

  List<Widget> _widgetOptions = <Widget>[
    Dasboard(),
    BookingList(),
    Calender(),
    TimeSheets()
  ];

  @override
  void initState() {
    getUserDashboard();
    super.initState();
  }

  getUserDashboard() async {
    User instance = User();
    var res = await instance.getUserDashboard();
    var data = json.decode(res.body);

    setState(() {
      _dashboard = data;
      if (_dashboard['todaysBooking'].length == 0) {
        _todaysBook = _dashboard['todaysBooking'];
      } else {
        _todaysBook.add(_dashboard['todaysBooking']);
      }
      _onItemTap(widget.index);
    });
  }

  void _onItemTap(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  navigateToProfile() {
    setState(() {
      _selectedIndex = 2;
      _onItemTap(_selectedIndex);
    });
  }

  navigateToNotification() {
    setState(() {
      _selectedIndex = 3;
      _onItemTap(_selectedIndex);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: getColorFromHex('#0b1a3b'),
      appBar: CustomAppBar(true, true, _selectedIndex),
      body: _body(),
      bottomNavigationBar: bottomNavBar(),
    );
  }

  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  Widget _profileIcon() {
    return GestureDetector(
      onTap: () {
        navigateToProfile();
      },
      child: Icon(
        Icons.account_circle_sharp,
        size: 35.0,
        color: Colors.white,
      ),
    );
  }

  Widget _title() {
    return Text(
      'n.',
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Colors.white,
        fontSize: 30.0,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  Widget _notificationIcon() {
    return GestureDetector(
      onTap: () {
        navigateToNotification();
      },
      child: Icon(
        Icons.circle_notifications,
        size: 35.0,
        color: Colors.white,
      ),
    );
  }

  PreferredSizeWidget _header() {
    return PreferredSize(
      child: SafeArea(
        child: Container(
          child: Padding(
            padding: EdgeInsets.all(20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[_profileIcon(), _title(), _notificationIcon()],
            ),
          ),
        ),
      ),
      preferredSize: Size.fromHeight(100),
    );
  }

  _body() {
    if (_todaysBook.length > 0) {
      var _publicId = _dashboard['todaysBooking'] != null
          ? _dashboard['todaysBooking']['publicId']
          : null;

      _widgetOptions = <Widget>[Dasboard(), Booking(_publicId), Calender()];
    }
    return Center(
      child: _widgetOptions.elementAt(_selectedIndex),
    );
  }

  Widget bottomNavBar() {
    return BottomNavigationBar(
      elevation: 0.0,
      backgroundColor: getColorFromHex('e2e7f0'),
      selectedItemColor: Colors.black,
      unselectedItemColor: Colors.grey,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(
            Icons.home,
            size: 30.0,
          ),
          title: Text(
            'Home',
          ),
        ),
        if (_todaysBook.length == 0)
          BottomNavigationBarItem(
            icon: Icon(
              Icons.message,
              size: 30.0,
            ),
            title: Text(
              'Booking List',
            ),
          ),
        if (_todaysBook.length > 0)
          BottomNavigationBarItem(
            icon: Icon(
              Icons.message,
              size: 30.0,
            ),
            title: Text(
              'Todays Booking',
            ),
          ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.date_range,
            size: 30.0,
          ),
          title: Text(
            'Calendar',
          ),
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.date_range,
            size: 30.0,
          ),
          title: Text(
            'Timesheets',
          ),
        ),
        // BottomNavigationBarItem(
        //   icon: Icon(
        //     Icons.account_circle,
        //     size: 30.0,
        //   ),
        //   title: Text(
        //     'Profile',
        //   ),
        // ),
        // BottomNavigationBarItem(
        //   icon: Icon(
        //     Icons.notifications,
        //     size: 30.0,
        //   ),
        //   title: Text(
        //     'Notifications',
        //   ),
        // ),
        // BottomNavigationBarItem(
        //   icon: Icon(
        //     Icons.date_range,
        //     size: 30.0,
        //   ),
        //   title: Text(
        //     'Calendar',
        //   ),
        // ),
      ],
      currentIndex: _selectedIndex,
      onTap: _onItemTap,
      selectedFontSize: 13.0,
      unselectedFontSize: 13.0,
      type: BottomNavigationBarType.fixed,
    );
  }
}
