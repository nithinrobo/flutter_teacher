import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:supplywell_teacher_app/src/screens/booking_list.dart';
import 'package:supplywell_teacher_app/src/screens/calendar.dart';
import 'package:supplywell_teacher_app/src/screens/dashboard.dart';
import 'package:supplywell_teacher_app/src/screens/nav.dart';
import 'package:supplywell_teacher_app/src/screens/notifications.dart';
import 'package:supplywell_teacher_app/src/screens/profile.dart';
import 'package:supplywell_teacher_app/src/screens/school_profile.dart';
import 'package:supplywell_teacher_app/src/services/booking.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';
import 'package:supplywell_teacher_app/src/services/users.dart';
import 'package:supplywell_teacher_app/src/widgets/app_header.dart';

class Booking extends StatefulWidget {
  String publicId;

  Booking(this.publicId);

  @override
  _BookingState createState() => _BookingState();
}

class _BookingState extends State<Booking> {
  var _bookingDetails = {}, _dashboard = {};
  bool isLoading = false;
  var _selectedIndex = 1;
  List _upcomingBooks = [], _todaysBook = [];
  var _bottomTapped = false;
  List pages = [Dasboard(), BookingList(), Calender()];

  @override
  void initState() {
    getBooking();
    getUserDashboard();
    super.initState();
  }

  getUserDashboard() async {
    User instance = User();
    isLoading = true;
    var res = await instance.getUserDashboard();
    isLoading = false;
    var data = json.decode(res.body);

    setState(() {
      _dashboard = data;
      _upcomingBooks = _dashboard['upcomingBookings'];
      if (_dashboard['todaysBooking'].length == 0) {
        _todaysBook = _dashboard['todaysBooking'];
      } else {
        _todaysBook.add(_dashboard['todaysBooking']);
      }
    });
  }

  getBooking() async {
    Bookings instance = Bookings();
    isLoading = true;
    var res = await instance.getBooking({'publicId': widget.publicId});
    isLoading = false;
    if (res.statusCode == 200) {
      setState(() {
        _bookingDetails = json.decode(res.body);
      });
    }
  }

  onSchoolProfileTap() {
    var _publicId = _bookingDetails['school'] != null
        ? _bookingDetails['school']['publicId']
        : '';
    Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => SchoolProfile(_publicId)),
    );
  }

  navigateToProfile() {
    Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => Nav(2)),
    );
  }

  navigateToNotification() {
    Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => Nav(3)),
    );
  }

  formatDate(date) {
    return Jiffy(date).format('do-MMMM-yyyy');
  }

  _getText(text, fontSize, color) {
    return Text(
      '$text',
      style: TextStyle(
        color: getColorFromHex(color),
        fontWeight: FontWeight.w500,
        fontSize: fontSize,
      ),
    );
  }

  getFormattedTime(time) {
    return Jiffy(time).format('hh:mm a');
  }

  //helper widgets
  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: getColorFromHex('#e2e7f0'),
      appBar: (_todaysBook.length == 0 && !isLoading)
          ? CustomAppBar(true, true, _selectedIndex)
          : null,
      body: _getBody(),
      bottomNavigationBar:
          (_todaysBook.length == 0 && !isLoading) ? _getBottomNav() : null,
    );
  }

  Widget _getBottomNav() {
    return BottomNavigationBar(
      elevation: 0.0,
      type: BottomNavigationBarType.fixed,
      backgroundColor: getColorFromHex('e2e7f0'),
      selectedItemColor: Colors.black,
      unselectedItemColor: Colors.grey,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(
            Icons.home,
          ),
          title: Text(
            'Home',
          ),
        ),
        if (_todaysBook.length == 0)
          BottomNavigationBarItem(
            icon: Icon(
              Icons.message,
            ),
            title: Text(
              'Bookings',
            ),
          ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.date_range,
          ),
          title: Text(
            'Calendar',
          ),
        ),
      ],
      currentIndex: _selectedIndex,
      onTap: _onItemTap,
      selectedFontSize: 13.0,
      unselectedFontSize: 13.0,
    );
  }

  Widget getProfileIcon() {
    return GestureDetector(
      onTap: () {
        navigateToProfile();
      },
      child: Icon(
        Icons.account_circle_sharp,
        size: 30.0,
        color: Colors.white,
      ),
    );
  }

  Widget getTitle() {
    return Text(
      'n.',
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Colors.white,
        fontSize: 30.0,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  Widget getNotificationIcon() {
    return GestureDetector(
      onTap: () {
        navigateToNotification();
      },
      child: Icon(
        Icons.circle_notifications,
        size: 30.0,
        color: Colors.white,
      ),
    );
  }

  Widget getSchool() {
    var school = _bookingDetails['school'] != null
        ? _bookingDetails['school']['name']
        : '';
    return Flexible(
      child: Text(
        school,
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
        style: TextStyle(
          fontSize: 18.0,
          color: getColorFromHex('#0b1a3b'),
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  getColorForStatusBox() {
    return getColorFromHex('c0e0cf');
  }

  getStatusName() {
    var _status = (_bookingDetails['request'] != null &&
            _bookingDetails['request']['confirmation'] == 1)
        ? 'Confirmed'
        : 'Not Confirmed';
    return _status;
  }

  Widget getStatus() {
    return Container(
      padding: EdgeInsets.all(5.0),
      decoration: BoxDecoration(
        color: getColorForStatusBox(),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Text(
        getStatusName(),
        textAlign: TextAlign.right,
        style: TextStyle(
          fontWeight: FontWeight.w500,
          fontFamily: 'Akkurat Pro Regular',
          fontSize: 15.0,
          color: Colors.black,
        ),
      ),
    );
  }

  Widget getDate() {
    var _startDate = (_bookingDetails['startDate'] != null &&
            _bookingDetails['startDate']['date'] != null)
        ? formatDate(_bookingDetails['startDate']['date'])
        : '';
    var _endDate = (_bookingDetails['endDate'] != null &&
            _bookingDetails['endDate']['date'] != null)
        ? formatDate(_bookingDetails['endDate']['date'])
        : '';
    var _mergedDate = '$_startDate - $_endDate';
    return Text(
      _mergedDate,
      textAlign: TextAlign.left,
      style: TextStyle(
        color: getColorFromHex('595959'),
        fontFamily: 'Akkurat Pro Regular',
        fontWeight: FontWeight.w600,
      ),
    );
  }

  Widget getDesc() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [getSchool(), getStatus()],
    );
  }

  Widget getParagraph() {
    return Text(
      'Here is the key information about this booking. For further information, please contact the School using the details below.',
      style: TextStyle(
        color: getColorFromHex('595959'),
        fontFamily: 'Akkurat Pro Regular',
        fontSize: 15.0,
        fontWeight: FontWeight.w600,
      ),
    );
  }

  getStartTimeData() {
    var _time = _bookingDetails['startTime'] != null
        ? _bookingDetails['startTime']['date']
        : '';
    var _startTime = getFormattedTime(_time);
    return _startTime;
  }

  getEndTimeData() {
    var _time = _bookingDetails['endTime'] != null
        ? _bookingDetails['endTime']['date']
        : '';
    var _startTime = getFormattedTime(_time);
    return _startTime;
  }

  getStartTime() {
    return Container(
        padding: EdgeInsets.all(20.0),
        decoration: BoxDecoration(
          color: getColorFromHex('0b1a3b'),
          border: Border.all(
            color: Colors.grey,
            width: 1,
          ),
          borderRadius: BorderRadius.circular(8),
        ),
        child: Column(
          children: [
            Text(
              'Start time',
              style: TextStyle(color: Colors.white),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              getStartTimeData(),
              textAlign: TextAlign.right,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15.0,
                color: Colors.white,
              ),
            ),
          ],
        ));
  }

  getEndTime() {
    return Container(
        padding: EdgeInsets.all(20.0),
        decoration: BoxDecoration(
          color: getColorFromHex('0b1a3b'),
          border: Border.all(
            color: Colors.grey,
            width: 1,
          ),
          borderRadius: BorderRadius.circular(8),
        ),
        child: Column(
          children: [
            Text(
              'End time',
              style: TextStyle(color: Colors.white),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              getEndTimeData(),
              textAlign: TextAlign.right,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15.0,
                color: Colors.white,
              ),
            ),
          ],
        ));
  }

  Widget getWorkingHours() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            SizedBox(
              width: 10.0,
            ),
            _getText('WORKING HOURS', 14.0, '#bdbdd1'),
          ],
        ),
        SizedBox(
          height: 10.0,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(child: getStartTime()),
            SizedBox(
              width: 10.0,
            ),
            Expanded(child: getEndTime())
          ],
        )
      ],
    );
  }

  Widget _getPageTitle() {
    return Center(
      child: Text(
        'Booking Info',
        style: TextStyle(
          fontSize: 33.0,
          color: getColorFromHex('0b1a3b'),
          letterSpacing: 1.0,
          fontFamily: 'Akkurat Pro Regular',
        ),
      ),
    );
  }

  Widget _showHoriZontalLine(height) {
    return Divider(
      color: getColorFromHex('#595959'),
      height: height,
      thickness: 1.0,
    );
  }

  Widget _showKeyValuePair(label, value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          child: Text(
            '$label',
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontFamily: 'Akkurat Pro Regular',
              fontSize: 16.0,
              color: getColorFromHex('595959'),
            ),
          ),
        ),
        Flexible(
          child: Text(
            '$value',
            overflow: TextOverflow.ellipsis,
            maxLines: 2,
            textAlign: TextAlign.right,
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 14.0,
              color: getColorFromHex('0b1a3b'),
            ),
          ),
        ),
      ],
    );
  }

  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

  _getSectionTitle(section) {
    var _title;
    if (section == 'confirmedIndividual') {
      _title = 'Confirmed individual';
    } else if (section == 'contactInfo') {
      _title = 'Contact information';
    } else if (section == 'WorkingHours') {
      _title = 'Working hours';
    } else if (section == 'roleInfo') {
      _title = 'Role information';
    } else if (section == 'schoolInfo') {
      _title = 'School information';
    } else {
      _title = 'Request Confirmed!';
    }
    return _title;
  }

  _sectionTitle(section) {
    var title = _getSectionTitle(section);
    return Text(
      title,
      textAlign: TextAlign.left,
      style: TextStyle(
        fontSize: 16.0,
        fontWeight: FontWeight.w600,
        fontFamily: 'Akkurat Pro Bold',
        color: getColorFromHex('0b1a3b'),
      ),
    );
  }

  Widget _confirmedIndividualData() {
    var _request =
        (_bookingDetails['request'] != null) ? _bookingDetails['request'] : {};
    var _subjectDetails = (_bookingDetails['primarySubject'] != null)
        ? _bookingDetails['primarySubject']
        : {};
    return Column(
      children: <Widget>[
        SizedBox(
          height: 5.0,
        ),
        _showKeyValuePair('Name', _request['name']),
        if (_request['email'] != null)
          SizedBox(
            height: 5.0,
          ),
        if (_request['email'] != null)
          _showKeyValuePair('Email', _request['email']),
        if (_subjectDetails['name'] != null)
          SizedBox(
            height: 5.0,
          ),
        if (_subjectDetails['name'] != null)
          _showKeyValuePair('Role', _subjectDetails['name']),
      ],
    );
  }

  Widget _contactInfoData() {
    var _school =
        _bookingDetails['school'] != null ? _bookingDetails['school'] : {};
    var _contact =
        _bookingDetails['contact'] != null ? _bookingDetails['contact'] : {};
    return Column(
      children: <Widget>[
        SizedBox(
          height: 5.0,
        ),
        _showKeyValuePair('school', _school['name']),
        SizedBox(
          height: 5.0,
        ),
        _showKeyValuePair('mobile', _contact['mobileNumber']),
      ],
    );
  }

  Widget _roleInfoData() {
    var _subjectDetails = _bookingDetails['primarySubject'] != null
        ? _bookingDetails['primarySubject']
        : '';
    var _urgent =
        _bookingDetails['urgent'] == 'true' || _bookingDetails['urgent'] == true
            ? 'yes'
            : 'no';

    var _workSet = _bookingDetails['workSet'] == 'true' ||
            _bookingDetails['workSet'] == true
        ? 'yes'
        : 'no';

    return Column(
      children: <Widget>[
        SizedBox(
          height: 5.0,
        ),
        _showKeyValuePair('Urgent', _urgent),
        SizedBox(
          height: 5.0,
        ),
        _showKeyValuePair('Key stage', _bookingDetails['keyStage']),
        SizedBox(
          height: 5.0,
        ),
        _showKeyValuePair('Primary Subject', _subjectDetails['name']),
        SizedBox(
          height: 5.0,
        ),
        _showKeyValuePair('Work set', _workSet)
      ],
    );
  }

  _workingHrsTemplate(label, value) {
    var _color = (label == 'Start time') ? 'c0e0cf' : 'f7c6b7';
    return Row(
      children: [
        Container(
          child: Text(
            '$label:',
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontFamily: 'Akkurat Pro Regular',
              fontSize: 15.0,
              color: getColorFromHex('595959'),
            ),
          ),
        ),
        if (label == 'Start time')
          SizedBox(
            width: 10.0,
          )
        else
          SizedBox(
            width: 18.0,
          ),
        Container(
          padding: EdgeInsets.all(5.0),
          decoration: BoxDecoration(
              color: getColorFromHex(_color),
              borderRadius: BorderRadius.circular(4.0)),
          child: Text(
            '$value',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 15.0,
              color: getColorFromHex('0b1a3b'),
            ),
          ),
        ),
      ],
    );
  }

  Widget _workingHrsData() {
    return Column(
      children: <Widget>[
        SizedBox(height: 5.0),
        _workingHrsTemplate('Start time', getStartTimeData()),
        SizedBox(height: 5.0),
        _workingHrsTemplate('End time', getEndTimeData())
      ],
    );
  }

  Widget _schoolInfoData() {
    return FlatButton(
      padding: EdgeInsets.all(20.0),
      minWidth: MediaQuery.of(context).size.width,
      onPressed: null,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'View School Profile',
            style: TextStyle(
              color: getColorFromHex('0b1a3b'),
              fontFamily: 'Akkurat Pro Bold',
              fontSize: 20.0,
            ),
          ),
          SvgPicture.asset(
            'assets/arrow-right-small.svg',
            width: 20.0,
            height: 20.0,
          )
        ],
      ),
      textColor: Colors.white,
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: getColorFromHex('0b1a3b'),
          width: 1,
          style: BorderStyle.solid,
        ),
        borderRadius: BorderRadius.circular(4),
      ),
    );
  }

  Widget _getDataSection(section) {
    var _dataSection;
    if (section == 'confirmedIndividual') {
      _dataSection = _confirmedIndividualData();
    } else if (section == 'contactInfo') {
      _dataSection = _contactInfoData();
    } else if (section == 'WorkingHours') {
      _dataSection = _workingHrsData();
    } else if (section == 'roleInfo') {
      _dataSection = _roleInfoData();
    } else {
      _dataSection = _schoolInfoData();
    }
    return _dataSection;
  }

  Widget _showSection(section) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _sectionTitle(section),
        if (section == 'schoolInfo') SizedBox(height: 5.0),
        _getDataSection(section),
        SizedBox(
          height: 12.0,
        ),
        _showHoriZontalLine(1.0),
        if (section != 'schoolInfo')
          SizedBox(
            height: 8.0,
          )
        else
          SizedBox(
            height: 4.0,
          )
      ],
    );
  }

  Widget _confirmedSection() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        _sectionTitle('confirmedReq'),
        SvgPicture.asset(
          'assets/green_tick_circle.svg',
          width: 40.0,
          height: 30.0,
        ),
      ],
    );
  }

  Widget getBody() {
    return Container(
      padding: EdgeInsets.all(20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 5.0),
          _getPageTitle(),
          SizedBox(height: 15.0),
          getDesc(),
          SizedBox(
            height: 10.0,
          ),
          getDate(),
          SizedBox(
            height: 5.0,
          ),
          getParagraph(),
          SizedBox(
            height: 12.0,
          ),
          _showHoriZontalLine(1.0),
          SizedBox(
            height: 8.0,
          ),
          _showSection('confirmedIndividual'),
          _showSection('contactInfo'),
          _showSection('WorkingHours'),
          _showSection('roleInfo'),
          GestureDetector(
            onTap: () {
              onSchoolProfileTap();
            },
            child: _showSection('schoolInfo'),
          ),
          _confirmedSection(),
          Container(
            child: Text(
              'You have been accepted',
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontFamily: 'Akkurat Pro Regular',
                fontSize: 15.0,
                color: getColorFromHex('595959'),
              ),
            ),
          ),
          SizedBox(
            height: 3.0,
          ),
          Text(
            'for this role',
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontFamily: 'Akkurat Pro Regular',
              fontSize: 15.0,
              color: getColorFromHex('595959'),
            ),
          ),
        ],
      ),
    );
  }

  void _onItemTap(int index) {
    setState(() {
      _bottomTapped = true;
      _selectedIndex = index;
    });
  }

  _getBody() {
    if (_todaysBook.length > 0) {
      pages = [Dasboard(), Calender()];
    }
    return isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : (_bottomTapped)
            ? pages[_selectedIndex]
            : SingleChildScrollView(child: getBody());
  }

  PreferredSizeWidget _header() {
    return PreferredSize(
      child: SafeArea(
        child: Container(
          color: getColorFromHex('0b1a3b'),
          child: Padding(
            padding: EdgeInsets.all(20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                getTitle(),
              ],
            ),
          ),
        ),
      ),
      preferredSize: Size.fromHeight(100),
    );
  }
}
