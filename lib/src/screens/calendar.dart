import 'package:flutter/material.dart';
import 'package:supplywell_teacher_app/src/widgets/app_header.dart';
import 'package:supplywell_teacher_app/src/widgets/day_picker.dart';
import 'package:supplywell_teacher_app/src/widgets/month_picker.dart';

class Calender extends StatefulWidget {
  @override
  _CalenderState createState() => _CalenderState();
}

class _CalenderState extends State<Calender> {
  var _selectedTab;
  final List<Widget> datePickers = <Widget>[DayPickerPage(), MonthPickerPage()];
  @override
  Widget build(BuildContext context) {
    Color getColorFromHex(String hexColor) {
      hexColor = hexColor.toUpperCase().replaceAll('#', '');

      if (hexColor.length == 6) {
        hexColor = 'FF' + hexColor;
      }

      return Color(int.parse(hexColor, radix: 16));
    }

    return Scaffold(
      backgroundColor: getColorFromHex('e2e7f0'),
      // appBar: CustomAppBar(false, true, 0),
      body: SingleChildScrollView(
        child: Container(
          child: DayPickerPage(),
        ),
      ),
    );
  }

//  TODO- dont remove this code
//   tabRelated() {
//     return DefaultTabController(
//       length: 2,
//       child: Scaffold(
//         appBar: CustomAppBar(false, true, 0),
//         body: SingleChildScrollView(
//           child: Container(
//             child: Column(
//               children: [
//                 Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: Container(
//                     decoration: BoxDecoration(
//                       color: getColorFromHex('#eaebf4'),
//                       borderRadius: BorderRadius.circular(10),
//                     ),
//                     child: TabBar(
//                       unselectedLabelColor: Colors.black,
//                       indicator: BoxDecoration(
//                         color: getColorFromHex('51dbf2'),
//                         borderRadius: BorderRadius.circular(8.0),
//                       ),
//                       tabs: [
//                         Tab(icon: Icon(Icons.date_range), text: 'day'),
//                         Tab(
//                           icon: Icon(Icons.date_range),
//                           text: 'month',
//                         )
//                       ],
//                     ),
//                   ),
//                 ),
//                 SizedBox(
//                   height: MediaQuery.of(context).size.height,
//                   child: TabBarView(
//                     children: [
//                       Container(
//                         child: DayPickerPage(),
//                       ),
//                       Container(
//                         child: MonthPickerPage(),
//                       )
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }
}
