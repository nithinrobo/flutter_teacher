import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:supplywell_teacher_app/src/screens/home.dart';
import 'package:supplywell_teacher_app/src/services/auth.dart';
import 'package:supplywell_teacher_app/src/widgets/app_header.dart';
import 'package:supplywell_teacher_app/src/widgets/custom_font.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  var _email;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getColorFromHex('e2e7f0'),
      appBar: CustomAppBar(false, false, 1),
      body: _getBody(),
    );
  }

  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  _customFontStyle(text, fontFamily, color, textAlignment, fontSize, fontWeight,
      letterSpacing) {
    return CustomFont(text, fontFamily, fontWeight, textAlignment, fontSize,
        color, letterSpacing);
  }

  Widget _getPageHeading() {
    return _customFontStyle('Reset your password', 'Akkurat Pro Bold', '0b1a3b',
        TextAlign.center, 21.0, FontWeight.w500, 1.0);
  }

  Widget _getStep1() {
    var _msg = 'Step 1 - Enter the email address you registered with below';
    return _customFontStyle(_msg, 'Akkurat Pro Regular', '0b1a3b',
        TextAlign.left, 16.0, FontWeight.w500, null);
  }

  Widget _getStep2() {
    var _msg =
        'Step 2 - Check your email and click the link to receive a new temporary password.';
    return _customFontStyle(_msg, 'Akkurat Pro Regular', '0b1a3b',
        TextAlign.left, 16.0, FontWeight.w500, null);
  }

  InputDecoration _formFieldDecoration(fieldName) {
    return InputDecoration(
      labelText: fieldName,
      filled: true,
      fillColor: getColorFromHex('#ffffff'),
      border: OutlineInputBorder(),
    );
  }

  Widget _emailField() {
    return TextFormField(
      decoration: _formFieldDecoration('Email Address'),
      validator: (String? value) {
        if (value != null && value.isEmpty) {
          return 'Email is Required';
        }

        if (value != null &&
            !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                .hasMatch(value)) {
          return 'Please enter a valid email Address';
        }

        return null;
      },
      onSaved: (String? value) {
        if (value != null) _email = value;
      },
    );
  }

  Widget _getBtnTitle(title, color) {
    return _customFontStyle(title, 'Akkurat Pro Regular', color,
        TextAlign.center, 20.0, FontWeight.w400, null);
  }

  onBtnClick() {
    //if the fields are not valid then just throw message
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      resetPassword();
    }
  }

  showFlutterToaster(msg, color) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.TOP,
      timeInSecForIosWeb: 3,
      backgroundColor: getColorFromHex(color),
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  resetPassword() async {
    Auth instance = Auth();

    var data = {'email': _email};

    var res = await instance.forgotPassword(data);
    if (res.statusCode == 200) {
      var body = json.decode(res.body);
      if (body['success'] != null) {
        showFlutterToaster(body['success'], 'c0e0cf');

        Navigator.push(
          context,
          new MaterialPageRoute(builder: (context) => Home()),
        );
      } else {
        showFlutterToaster(body['error'], 'db4437');
      }
    }
  }

  Widget _submitBtn() {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: getColorFromHex('0b1a3b'), // background
        padding: EdgeInsets.all(20.0),
        elevation: 0.0,
        shadowColor: Colors.transparent,
      ),
      child: _getBtnTitle('Reset Password', 'ffffff'),
      onPressed: onBtnClick,
    );
  }

  Widget _getFormField() {
    return Form(
      key: _formKey,
      child: Column(
        children: [_emailField(), SizedBox(height: 20.0), _submitBtn()],
      ),
    );
  }

  Widget _getBody() {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Column(children: [
        SizedBox(height: 10.0),
        _getPageHeading(),
        SizedBox(height: 20.0),
        _getStep1(),
        SizedBox(height: 10.0),
        _getStep2(),
        SizedBox(height: 10.0),
        Padding(padding: EdgeInsets.all(10.0), child: _getFormField()),
        SizedBox(height: 20.0),
      ]),
    );
  }
}
