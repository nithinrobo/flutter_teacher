import 'package:flutter/material.dart';
import 'package:supplywell_teacher_app/src/screens/nav.dart';
import 'package:supplywell_teacher_app/src/widgets/app_header.dart';

class TimesheetContestComplete extends StatefulWidget {
  @override
  _TimesheetContestCompleteState createState() =>
      _TimesheetContestCompleteState();
}

class _TimesheetContestCompleteState extends State<TimesheetContestComplete> {
  onBtnClicked() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => Nav(0)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(true, true, 1),
      body: _getBody(),
    );
  }

  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  Widget _getText(text, fontSize, color) {
    return Text(
      '$text',
      style: TextStyle(
        color: getColorFromHex(color),
        fontWeight: FontWeight.w500,
        fontSize: fontSize,
      ),
    );
  }

  _geTtitle() {
    return _getText('Thank you for confirming this timesheet', 20.0, '0b1a3b');
  }

  _getDesc() {
    return _getText(
        "We'll be in touch soon with further information.", 14.0, '000000');
  }

  _getBtn() {
    return Container(
        width: MediaQuery.of(context).size.width,
        child: ElevatedButton(
          child: Text('Return to Dashboard'),
          onPressed: onBtnClicked,
          style: ElevatedButton.styleFrom(
            primary: getColorFromHex('c0e0cf'), // background
            padding: EdgeInsets.all(20.0),
            elevation: 0.0,
            shadowColor: Colors.transparent,
          ),
        ));
  }

  Widget _getBody() {
    return Container(
        padding: EdgeInsets.all(10.0),
        child: Column(children: [
          _geTtitle(),
          SizedBox(height: 10.0),
          _getDesc(),
          SizedBox(height: 20.0),
          _getBtn()
        ]));
  }
}
