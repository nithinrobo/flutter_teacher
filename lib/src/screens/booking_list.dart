import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:jiffy/jiffy.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:supplywell_teacher_app/src/screens/booking.dart';
import 'package:supplywell_teacher_app/src/screens/home.dart';
import 'package:supplywell_teacher_app/src/screens/nav.dart';
import 'package:supplywell_teacher_app/src/screens/notifications.dart';
import 'package:supplywell_teacher_app/src/screens/profile.dart';
import 'package:supplywell_teacher_app/src/services/booking.dart';
import 'package:supplywell_teacher_app/src/services/users.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class BookingList extends StatefulWidget {
  @override
  _BookingListState createState() => _BookingListState();
}

class _BookingListState extends State<BookingList> {
  var _fName, _userDetails, _dashboard = {}, _todaysBook = [];
  List _bookingList = [];

  late Timer timer;
  Map _bookingDetails = {};
  bool isLoading = false, hasBooked = false;
  var _status = '';

  @override
  void initState() {
    init();
    super.initState();
  }

  init() {
    getUserDashboard(true);
    getBookingsList();
    timer = Timer.periodic(
      Duration(seconds: 3),
      (Timer t) => (getBookingsList()),
    );
    getUserDetails();
    _fName = _getFirstName();
  }

  getBookingsList() async {
    Bookings instance = Bookings();
    var res = await instance.getBookingList();
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      setState(() {
        _bookingList = data;
      });
      var _today = Jiffy().format('dd/MM/yyyy');
      //call dashboard api, once booking is done, for today
      if (_todaysBook.length == 0) {
        getUserDashboard(false);
      } else {
        timer.cancel();

        Navigator.pushReplacement(
            context, new MaterialPageRoute(builder: (context) => Nav(1)));
      }
      //filter out the booking on current date
      _bookingList = data;
      // _bookingList.sort((a, b) =>
      //     b['startDate'].toString().compareTo(a['startDate'].toString()));
    } else if (res.statusCode == 401) {
      //if tokern expired, navigate to home page
      Navigator.push(
        context,
        new MaterialPageRoute(builder: (context) => Home()),
      );
    }
    if (!mounted) return;
  }

  getBooking(index) async {
    isLoading = true;
    Bookings instance = Bookings();
    var res = await instance
        .getBooking({'publicId': _bookingList[index]['publicId']});
    isLoading = false;
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      setState(() {
        _bookingDetails = data;
      });
      var req = _bookingDetails['request'];
      if (req['confirmation'] == null) {
        _cancelRequest(index);
      }
    }
  }

  getUserDashboard(showLoader) async {
    User instance = User();
    if (showLoader) isLoading = true;
    var res = await instance.getUserDashboard();
    if (showLoader) isLoading = false;
    var data = json.decode(res.body);

    setState(() {
      _dashboard = data;
      if (_dashboard['todaysBooking'].length == 0) {
        _todaysBook = _dashboard['todaysBooking'];
      } else {
        _todaysBook.add(_dashboard['todaysBooking']);
      }
    });
  }

  getUserDetails() async {
    User instance = User();
    var res = await instance.getUserDetails();
    var data = json.decode(res.body);
    if (!mounted) return;

    setState(() {
      _userDetails = data;
    });
  }

  getTkn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var _tkn = prefs.getString('token');
    return _tkn;
  }

  _getFirstName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _fName = prefs.getString('name').toString();
    return _fName;
  }

  apply(index, context) async {
    var data;

    final p = {
      'roleId': _bookingList[index]['publicId'].toString(),
    };

    isLoading = true;
    Bookings instance = Bookings();
    var res = await instance.createRoleRequest(p);

    isLoading = false;
    data = json.decode(res.body);

    if (res.statusCode == 200) {
      Navigator.of(context).pop();
    }
  }

  showFlutterToaster(msg) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.TOP,
      timeInSecForIosWeb: 3,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  onCancel(index) {
    var teacher = _bookingList[index]['teacher'];
    var req = _bookingList[index]['bookingRequest'];
    if (teacher == null) {
      Navigator.of(context).pop();
    } else {
      if (teacher == _fName) {
        _cancelRequest(index);
      } else {
        showFlutterToaster(
            'You dont have permission to cancel this request because, this is requested by someone else');
      }
    }
  }

  _cancelRequest(index) async {
    Bookings instance = Bookings();
    var p = {
      'requestId': _bookingList[index]['bookingRequest']['id'].toString()
    };

    var res = await instance.cancelRequest(p);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      Navigator.of(context).pop();
    }
  }

  //helper widgets
  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: getColorFromHex('#e2e7f0'),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : getBody(),
    );
  }

  Widget getProfileIcon() {
    return GestureDetector(
      onTap: () {
        navigateToProfile();
      },
      child: Icon(
        Icons.account_circle_sharp,
        size: 30.0,
        color: Colors.white,
      ),
    );
  }

  Widget getTitle() {
    return Text(
      'n.',
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Colors.white,
        fontSize: 30.0,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  Widget getNotificationIcon() {
    return GestureDetector(
      onTap: () {
        navigateToNotification();
      },
      child: Icon(
        Icons.circle_notifications,
        size: 30.0,
        color: Colors.white,
      ),
    );
  }

  Widget getBody() {
    return Container(
      padding: EdgeInsets.all(20),
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 5.0),
            getHeading(),
            SizedBox(height: 15.0),
            (!isLoading && _bookingList.length > 0)
                ? _getBookingList()
                : Center(
                    child: Text('No Bookings found'),
                  )
          ],
        ),
      ),
    );
  }

  Widget getHeading() {
    return Center(
      child: Text(
        _todaysBook.length > 0 && _todaysBook[0]['publicId'] != null
            ? 'Todays Booking'
            : 'Jobs',
        style: TextStyle(
          fontSize: 33.0,
          color: getColorFromHex('0b1a3b'),
          letterSpacing: 1.0,
          fontFamily: 'Akkurat Pro Regular',
        ),
      ),
    );
  }

  navigateToProfile() {
    timer.cancel();
    Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => Profile()),
    );
  }

  navigateToNotification() {
    timer.cancel();
    Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => Notifications()),
    );
  }

  Widget _getBookingList() {
    return ListView.builder(
      itemCount: _bookingList.length,
      shrinkWrap: true,
      primary: false,
      itemBuilder: (BuildContext context, index) => Container(
        child: (isLoading) ? Text('Loading,,,,') : _getCard(index),
      ),
    );
  }

  getBg() {
    if (_status == 'Booked by me') {
      return getColorFromHex('c0e0cf');
    } else {
      return getColorFromHex('fff');
    }
  }

  Widget _showHoriZontalLine(height) {
    return Divider(
      color: getColorFromHex('#595959'),
      height: height,
      thickness: 1.0,
    );
  }

  _getCard(index) {
    return GestureDetector(
      onTap: () {
        var title = '';
        var teacher = _bookingList[index]['teacher'];
        var request = _bookingList[index]['bookingRequest'];
        if (teacher != null) {
          if (request['confirmation'] != null) {
            title = (teacher != _fName)
                ? 'Unfortunately this role has been filled'
                : 'Congratulations!, You were Successful';
          } else {
            title = 'Request has been sent';
          }
        } else {
          title = 'Request this Role?';
        }

        if (title == 'Congratulations!, You were Successful') {
          timer.cancel();
          var path;
          if (_todaysBook.length == 0) {
            path = Booking(_bookingList[index]['publicId']);
          } else {
            path = Nav(1);
          }
          Navigator.push(
            context,
            new MaterialPageRoute(builder: (context) => path),
          );
        } else {
          showDialogFunc(context, title, _bookingList[index], index);
        }
      },
      child: Container(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _getStatus(index),
              SizedBox(height: 5.0),
              _getNurseryName(index),
              SizedBox(height: 5.0),
              _getJobType(index),
              SizedBox(height: 5.0),
              _getDate(index),
              SizedBox(height: 5.0),
              if (_status == 'Pending...') SizedBox(height: 5.0),
              if (_status == 'Pending...')
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Request sent',
                      style: TextStyle(
                        fontSize: 18.0,
                        fontFamily: 'Akkurat Pro Bold',
                        color: getColorFromHex('0b1a3b'),
                      ),
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    SvgPicture.asset(
                      'assets/request_sent_icon.svg',
                      width: 30.0,
                      height: 20.0,
                    )
                  ],
                ),
              if (_status == 'Booked by me') SizedBox(height: 5.0),
              if (_status == 'Booked by me')
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Booking Confirmed!',
                      style: TextStyle(
                        fontSize: 18.0,
                        fontFamily: 'Akkurat Pro Bold',
                        fontWeight: FontWeight.bold,
                        color: getColorFromHex('0b1a3b'),
                      ),
                    ),
                    SvgPicture.asset(
                      'assets/green_tick_circle.svg',
                      width: 30.0,
                      height: 30.0,
                    )
                  ],
                ),
              if (_status == 'Available')
                FlatButton(
                  padding: EdgeInsets.all(20.0),
                  color: getColorFromHex('c0e0cf'),
                  onPressed: () {
                    showDialogFunc(context, 'Request this role?',
                        _bookingList[index], index);
                  },
                  child: Text(
                    'Request Booking',
                    style: TextStyle(color: getColorFromHex('#333333')),
                  ),
                ),
              if (_status == 'Pending...')
                SizedBox(
                  height: 5.0,
                ),
              if (_status == 'Pending...')
                FlatButton(
                  padding: EdgeInsets.all(20.0),
                  color: getColorFromHex('fd4f57'),
                  onPressed: () {
                    showDialogFunc(
                        context, 'Are you sure?', _bookingList[index], index);
                  },
                  child: Text(
                    'Cancel request',
                    style: TextStyle(color: getColorFromHex('ffffff')),
                  ),
                ),
              SizedBox(
                height: 10.0,
              ),
              _showHoriZontalLine(2.0),
              SizedBox(
                height: 10.0,
              )
            ],
          ),
        ),
      ),
    );
  }

  _getStatus(index) {
    var teacher = _bookingList[index]['teacher'];
    var request = _bookingList[index]['bookingRequest'];
    if (teacher != null) {
      if (request['confirmation'] != null) {
        _status = (teacher == _fName) ? 'Booked by me' : 'Unavailable';
      } else {
        _status = 'Pending...';
      }
    } else {
      _status = 'Available';
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          child: Text(
            'Status',
            textAlign: TextAlign.left,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 15.0,
              fontFamily: 'Akkurat Pro Regular',
              color: getColorFromHex('666666'),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(5.0, 3.0, 5.0, 3.0),
          decoration: BoxDecoration(
            color: getColorForStatusBox(),
            borderRadius: BorderRadius.circular(8),
          ),
          child: Text(
            _status,
            textAlign: TextAlign.right,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 15.0,
              fontFamily: 'Akkurat Pro Regular',
              color: _status == 'Booked by me' ? Colors.white : Colors.black,
            ),
          ),
        ),
      ],
    );
  }

  Color getColorForStatusBox() {
    var _color = '';
    if (_status == 'Available') {
      _color = '#c0e0cf';
    } else if (_status == 'Unavailable') {
      _color = '#f7c6b7';
    } else if (_status == 'Pending...') {
      _color = '#DFE1E4';
    } else if (_status == 'Booked by me') {
      _color = '#0b1a3b';
    } else {
      _color = '#c0e0cf';
    }

    return getColorFromHex(_color);
  }

  _getNurseryName(index) {
    return _getTheInfo('Nursery', _bookingList[index]['school']);
  }

  _getJobType(index) {
    return _getTheInfo('Job', _bookingList[index]['subject']);
  }

  Widget _getDate(index) {
    var mergedDate = _bookingList[index]['startDate'] +
        ' - ' +
        _bookingList[index]['endDate'];
    return _getTheInfo('Date ', mergedDate);
  }

  getAlertHeight() {
    var _height;
    if (_status == 'Available') {
      _height = MediaQuery.of(context).size.height * 0.8;
    } else if (_status == 'Unavailable') {
      _height = 330.0;
    } else if (_status == 'Booked by me') {
      _height = 330.0;
    } else if (_status == 'Pending') {
      _height = 345.0;
    }
    return _height;
  }

  Widget _getTheInfo(label, value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          '$label',
          textAlign: TextAlign.left,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15.0,
            color: getColorFromHex('666666'),
            fontFamily: 'Akkurat Pro Bold',
          ),
        ),
        Flexible(
          child: Text(
            '$value',
            textAlign: TextAlign.right,
            overflow: TextOverflow.ellipsis,
            maxLines: 3,
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 13.0,
              fontFamily: 'Akkurat Pro Bold',
              color: getColorFromHex('0b1a3b'),
            ),
          ),
        )
      ],
    );
  }

  Widget getCancelBtnText(data) {
    var _text = '';
    if (data['teacher'] == null) {
      _text = 'Cancel';
    } else {
      _text = 'Cancel this role';
    }
    return Text('$_text');
  }

  _getDialogueHeight(desc) {
    if (desc['teacher'] == null ||
        desc['bookingRequest'] != null &&
            desc['bookingRequest']['confirmation'] == null) {
      return MediaQuery.of(context).size.height * 0.48;
    } else {
      return MediaQuery.of(context).size.height * 0.40;
    }
  }

  // This is a block of Model Dialog
  showDialogFunc(context, title, desc, index) {
    return showDialog(
      context: context,
      builder: (context) {
        return Padding(
          padding: const EdgeInsets.all(20.0),
          child: Center(
            child: Material(
              shadowColor: getColorFromHex('0b1a3b'),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                ),
                padding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      title,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 20,
                        color: getColorFromHex('0b1a3b'),
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Divider(
                      thickness: 1.0,
                      color: Colors.grey,
                    ),
                    Container(
                      child: Align(
                        alignment: Alignment.center,
                        child: Column(
                          children: [
                            SizedBox(height: 3.0),
                            SvgPicture.asset(
                              'assets/educator_icon.svg',
                              width: 40,
                              height: 30.0,
                            ),
                            SizedBox(height: 5.0),
                            Text(
                              desc['subject'],
                              maxLines: 3,
                              style: TextStyle(
                                fontSize: 18.0,
                                color: getColorFromHex('0b1a3b'),
                                fontWeight: FontWeight.w500,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(height: 4.0),
                            Text(
                              desc['school'],
                              maxLines: 3,
                              style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.grey[500],
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(height: 4.0),
                            Text(
                              "${desc['startDate']} - ${desc['endDate']}",
                              maxLines: 3,
                              style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.grey[500],
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(
                              height: 7,
                            ),
                            Divider(
                              thickness: 1.0,
                              color: Colors.grey,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            if (desc['teacher'] != null)
                              FlatButton(
                                padding: EdgeInsets.all(20.0),
                                color: getColorFromHex('c0e0cf'),
                                splashColor: Colors.black12,
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Text("Return to roles"),
                                minWidth: MediaQuery.of(context).size.width,
                              ),
                            if (desc['teacher'] == null)
                              SizedBox(
                                height: 5,
                              ),
                            if (desc['teacher'] == null)
                              FlatButton(
                                padding: EdgeInsets.all(18.0),
                                color: getColorFromHex('c0e0cf'),
                                splashColor: Colors.black12,
                                onPressed: () {
                                  apply(index, context);
                                },
                                child: Text("Apply for booking"),
                                minWidth: MediaQuery.of(context).size.width,
                              ),
                            if (desc['teacher'] == null ||
                                desc['bookingRequest'] != null &&
                                    desc['bookingRequest']['confirmation'] ==
                                        null)
                              SizedBox(
                                height: 20,
                              ),
                            if (desc['teacher'] == null ||
                                desc['bookingRequest'] != null &&
                                    desc['bookingRequest']['confirmation'] ==
                                        null)
                              FlatButton(
                                padding: EdgeInsets.all(18.0),
                                color: Colors.grey,
                                splashColor: Colors.black12,
                                onPressed: () {
                                  onCancel(index);
                                },
                                child: getCancelBtnText(desc),
                                minWidth: MediaQuery.of(context).size.width,
                              ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
