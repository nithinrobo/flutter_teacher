import 'package:flutter/material.dart';
import 'package:supplywell_teacher_app/src/screens/google_map.dart';
import 'package:supplywell_teacher_app/src/screens/nav.dart';
import 'package:supplywell_teacher_app/src/services/booking.dart';
import 'dart:convert';

import 'package:supplywell_teacher_app/src/widgets/app_header.dart';

class SchoolProfile extends StatefulWidget {
  String publicId;

  SchoolProfile(this.publicId);

  @override
  _SchoolProfileState createState() => _SchoolProfileState();
}

class _SchoolProfileState extends State<SchoolProfile> {
  var _schoolDetails = {};
  bool isLoading = false;

  @override
  void initState() {
    getSchool();
    super.initState();
  }

  getSchool() async {
    Bookings instance = Bookings();
    isLoading = true;
    var res = await instance.getSchool({'publicId': widget.publicId});
    isLoading = false;
    if (res.statusCode == 200) {
      setState(() {
        _schoolDetails = json.decode(res.body);
      });
    }
  }

  navigate() {
    Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => GoogleMapView(widget.publicId),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: getColorFromHex('#e2e7f0'),
      appBar: CustomAppBar(true, true, 1),
      body: isLoading ? Center(child: CircularProgressIndicator()) : _getBody(),
    );
  }

  navigateToProfile() {
    Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => Nav(2)),
    );
  }

  navigateToNotification() {
    Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => Nav(3)),
    );
  }

  Widget getProfileIcon() {
    return GestureDetector(
      onTap: () {
        navigateToProfile();
      },
      child: Icon(
        Icons.account_circle_sharp,
        size: 30.0,
        color: Colors.white,
      ),
    );
  }

  Widget getTitle() {
    return Text(
      'n.',
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Colors.white,
        fontSize: 30.0,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  Widget getNotificationIcon() {
    return GestureDetector(
      onTap: () {
        navigateToNotification();
      },
      child: Icon(
        Icons.circle_notifications,
        size: 30.0,
        color: Colors.white,
      ),
    );
  }

  PreferredSizeWidget _header() {
    return PreferredSize(
      child: SafeArea(
        child: Container(
          color: getColorFromHex('0b1a3b'),
          child: Padding(
            padding: EdgeInsets.all(20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                getProfileIcon(),
                getTitle(),
                getNotificationIcon()
              ],
            ),
          ),
        ),
      ),
      preferredSize: Size.fromHeight(100),
    );
  }

  Widget _getBody() {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _getText(_schoolDetails['name'], 18.0, '#0b1a3b'),
          SizedBox(
            height: 5.0,
          ),
          _getText(_schoolDetails['city'], 13.0, '#0b1a3b'),
          SizedBox(
            height: 10.0,
          ),
          _getGoogleMapSection(),
          SizedBox(
            height: 30.0,
          ),
          _getContactInfo()
        ],
      ),
    );
  }

  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  Widget _getText(text, fontSize, color) {
    return Text(
      '$text',
      style: TextStyle(
        color: getColorFromHex(color),
        fontWeight: FontWeight.w500,
        fontSize: fontSize,
      ),
    );
  }

  Widget _getContactInfoData(label, value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          child: Text(
            '$label',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 15.0,
              color: getColorFromHex('0b1a3b'),
            ),
          ),
        ),
        Container(
          child: Text(
            '$value',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 15.0,
              color: Colors.grey[500],
            ),
          ),
        ),
      ],
    );
  }

  Widget _getUpdateInfo(title, subtitle, bgColor, textColor, sideHeadingColor) {
    return Container(
      padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: getColorFromHex(bgColor),
        borderRadius: BorderRadius.circular(10),
      ),
      child: GestureDetector(
        onTap: () {},
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
              child: Text(
                '$title',
                style: TextStyle(
                  fontSize: 16.0,
                  color: getColorFromHex(textColor),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              child: Text(
                subtitle,
                style: TextStyle(
                  fontSize: 18.0,
                  color: getColorFromHex(sideHeadingColor),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getGoogleMapSection() {
    return GestureDetector(
      onTap: () {
        navigate();
      },
      child: _getUpdateInfo('VIEW IN MAP', 'View this school in google map',
          '#0b1a3b', '#bdbdd1', '#FFFFFF'),
    );
  }

  Widget _getDot(color) {
    return Container(
      width: 5.0,
      height: 5.0,
      decoration: BoxDecoration(
        color: getColorFromHex(color),
        borderRadius: BorderRadius.circular(12),
      ),
    );
  }

  Widget _getContactInfo() {
    return Container(
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(12),
        border: Border.all(color: getColorFromHex('eaebf4')),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              _getDot('#ff5f5f'),
              SizedBox(
                width: 10.0,
              ),
              _getText('CONTACT INFORMATION', 14.0, '#bdbdd1'),
            ],
          ),
          SizedBox(
            height: 5.0,
          ),
          _getText(_schoolDetails['name'], 18.0, '#0b1a3b'),
          SizedBox(
            height: 10.0,
          ),
          _getContactInfoData('Telephone', _schoolDetails['tel']),
          _getContactInfoData('Email', _schoolDetails['email']),
        ],
      ),
    );
  }
}
