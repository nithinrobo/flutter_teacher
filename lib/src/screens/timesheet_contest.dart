import 'package:flutter/material.dart';
import 'package:supplywell_teacher_app/src/screens/timesheet_contest_complete.dart';
import 'package:supplywell_teacher_app/src/services/timesheets.dart';
import 'package:supplywell_teacher_app/src/widgets/app_header.dart';

class TimesheetContest extends StatefulWidget {
  String hash;

  TimesheetContest(this.hash);

  @override
  _TimesheetContestState createState() => _TimesheetContestState();
}

class _TimesheetContestState extends State<TimesheetContest> {
  confirmTimesheet() async {
    TimeSheetService instance = TimeSheetService();
    var res = await instance.contestTimeSheet(widget.hash);
    if (res.statusCode == 200) {
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => TimesheetContestComplete()));
    }
  }

  onContestBtnClicked() {
    confirmTimesheet();
  }

  onBackBtnClicked() {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getColorFromHex('e2e7f0'),
      appBar: CustomAppBar(true, true, 1),
      body: _getBody(),
    );
  }

  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  Widget _getText(text, fontSize, color) {
    return Text(
      '$text',
      style: TextStyle(
        color: getColorFromHex(color),
        fontWeight: FontWeight.w500,
        fontSize: fontSize,
      ),
    );
  }

  _geTtitle() {
    return _getText(
        'Is Something In This Timesheet Incorrect?', 20.0, '0b1a3b');
  }

  _getDesc() {
    return _getText(
        "If something seems wrong in this timesheet, press the button below to let us know and we'll be in touch.",
        14.0,
        '000000');
  }

  _getBtns() {
    return Column(
      children: [
        Container(
            width: MediaQuery.of(context).size.width,
            child: ElevatedButton(
              child: Text('Contest this timesheet'),
              onPressed: onContestBtnClicked,
              style: ElevatedButton.styleFrom(
                primary: getColorFromHex('#ff5f5f'), // background
                padding: EdgeInsets.all(20.0),
                elevation: 0.0,
                shadowColor: Colors.transparent,
              ),
            )),
        SizedBox(
          height: 10.0,
        ),
        Container(
            width: MediaQuery.of(context).size.width,
            child: ElevatedButton(
              child: Text('Back'),
              onPressed: onBackBtnClicked,
              style: ElevatedButton.styleFrom(
                primary: getColorFromHex('c0e0cf'), // background
                padding: EdgeInsets.all(20.0),
                elevation: 0.0,
                shadowColor: Colors.transparent,
              ),
            )),
      ],
    );
  }

  Widget _getBody() {
    return Container(
        padding: EdgeInsets.all(10.0),
        child: Column(children: [
          _geTtitle(),
          SizedBox(height: 10.0),
          _getDesc(),
          SizedBox(height: 20.0),
          _getBtns()
        ]));
  }
}
