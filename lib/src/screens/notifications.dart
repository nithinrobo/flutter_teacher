import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:supplywell_teacher_app/src/screens/booking.dart';
import 'package:supplywell_teacher_app/src/screens/dashboard.dart';
import 'package:supplywell_teacher_app/src/screens/nav.dart';
import 'package:supplywell_teacher_app/src/screens/profile.dart';
import 'package:supplywell_teacher_app/src/services/users.dart';
import 'package:supplywell_teacher_app/src/widgets/app_header.dart';

class Notifications extends StatefulWidget {
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  var _notificationList = [];
  bool isLoading = false;
  var _dashboard = {};
  List _todaysBook = [];

  @override
  void initState() {
    init();
    super.initState();
  }

  init() {
    getNotificationList();
    getUserDashboard();
  }

  getUserDashboard() async {
    User instance = User();
    isLoading = true;
    var res = await instance.getUserDashboard();
    isLoading = false;
    var data = json.decode(res.body);

    setState(() {
      _dashboard = data;
      if (_dashboard['todaysBooking'].length == 0) {
        _todaysBook = _dashboard['todaysBooking'];
      } else {
        _todaysBook.add(_dashboard['todaysBooking']);
      }
    });
  }

  getNotificationList() async {
    User instance = User();
    isLoading = true;
    var res = await instance.getNotifications();
    isLoading = false;
    var data = json.decode(res.body);
    setState(() {
      _notificationList = data;
    });
  }

  navigate(index) {
    var arr = _notificationList[index]['url'].split('/');
    var path = _todaysBook.length == 0 ? Booking(arr[2]) : Nav(1);
    print('arr - $arr');
    if (arr[1] == 'booking') {
      Navigator.push(
        context,
        new MaterialPageRoute(builder: (context) => path),
      );
    }
  }

  onClick(index) {
    navigate(index);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: CustomAppBar(true, true, 1),
      backgroundColor: getColorFromHex('#e2e7f0'),
      body: isLoading ? showLoader() : getBody(),
    );
  }

  Widget showLoader() {
    return Center(child: CircularProgressIndicator());
  }

  //helper widgets
  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  //whole body shown here
  Widget getBody() {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
            SizedBox(height: 5.0),
            getPageTitle(),
            SizedBox(height: 5.0),
            (_notificationList.length > 1 && !isLoading)
                ? _getNotificationList()
                : Center(
                    child: Text('No notifications found'),
                  ),
          ],
        ),
      ),
    );
  }

  Widget getPageTitle() {
    return getHeading();
  }

  Widget getHeading() {
    return Center(
      child: Text(
        'Notifications',
        style: TextStyle(
          fontSize: 33.0,
          color: getColorFromHex('0b1a3b'),
          letterSpacing: 1.0,
          fontFamily: 'Akkurat Pro Regular',
        ),
      ),
    );
  }

  Widget _getNotificationList() {
    return ListView.builder(
      itemCount: _notificationList.length,
      shrinkWrap: true,
      primary: false,
      itemBuilder: (BuildContext context, index) => Container(
        child: (isLoading) ? Text('Loading,,,,') : _getCard(index),
      ),
    );
  }

  Widget _getCard(index) {
    return GestureDetector(
      onTap: () {
        onClick(index);
      },
      child: Container(
        padding: EdgeInsets.all(15.0),
        child: Column(
          children: [
            getTimes(index),
            SizedBox(
              height: 10.0,
            ),
            getMessage(index)
          ],
        ),
      ),
    );
  }

  doFirstLetterCapital(data) {
    return '${data[0].toUpperCase()}${data.substring(1)}';
  }

  Widget getNotificationType(index) {
    var word = doFirstLetterCapital(_notificationList[index]['type']);
    return Text(
      word,
      style: TextStyle(
        fontFamily: 'Akkurat Pro Bold',
        fontWeight: FontWeight.bold,
        color: getColorFromHex('0b1a3b'),
      ),
    );
  }

  Widget getRecievedDate(index) {
    return Text(
      _notificationList[index]['time'],
      style: TextStyle(
        fontFamily: 'Akkurat Pro Regular',
      ),
    );
  }

  Widget getTimes(index) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [getNotificationType(index), getRecievedDate(index)]);
  }

  Widget getMessage(index) {
    return Text(
      _notificationList[index]['message'],
      style: TextStyle(fontFamily: 'Akkurat Pro Regular'),
    );
  }
}
