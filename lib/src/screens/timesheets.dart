import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:supplywell_teacher_app/src/screens/timesheet.dart';
import 'package:supplywell_teacher_app/src/services/timesheets.dart';
import 'package:supplywell_teacher_app/src/widgets/app_header.dart';

class TimeSheets extends StatefulWidget {
  @override
  _TimeSheetsState createState() => _TimeSheetsState();
}

class _TimeSheetsState extends State<TimeSheets> {
  var _timeSheetData, confirmedTimeSheets = [], unConfirmedTimeSheets = [];
  bool isLoading = false;
  @override
  void initState() {
    init();
    super.initState();
  }

  init() async {
    isLoading = true;
    TimeSheetService instance = TimeSheetService();
    var res = await instance.getTimeSheets();
    isLoading = false;
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      setState(() {
        _timeSheetData = data;
        unConfirmedTimeSheets = _timeSheetData['unconfirmed'];
        confirmedTimeSheets = _timeSheetData['confirmed'];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: getColorFromHex('e2e7f0'),
      body: (isLoading)
          ? Center(
              child: CircularProgressIndicator(),
            )
          : _getBody(),
    );
  }

  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  Widget _getUpdateInfo(title, subtitle, bgColor, textColor, sideHeadingColor) {
    return Container(
      padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: getColorFromHex(bgColor),
        borderRadius: BorderRadius.circular(10),
      ),
      child: GestureDetector(
        onTap: () {},
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
              child: Text(
                '$title',
                style: TextStyle(
                  fontSize: 16.0,
                  color: getColorFromHex(textColor),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              child: Text(
                subtitle,
                style: TextStyle(
                  fontSize: 18.0,
                  color: getColorFromHex(sideHeadingColor),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _getUnConfirmedBlock() {
    var desc =
        'You have ${unConfirmedTimeSheets.length} unconfirmed timesheets, Please confirm them to receive payment';
    return _getUpdateInfo(
        'UNCONFIRMED TIMESHEETS', desc, '#ff5f5f', '#ffffff', '#ffffff');
  }

  _getconfirmedBlock() {
    var desc =
        'You have ${confirmedTimeSheets.length} unconfirmed timesheets, Please confirm them to receive payment';
    return _getUpdateInfo(
        'CONFIRMED TIMESHEETS', desc, '#c0e0cf', '#ffffff', '#ffffff');
  }

  Widget getPageTitle() {
    return Text(
      'Your Timesheets',
      textAlign: TextAlign.left,
      style: TextStyle(
        fontSize: 20.0,
        color: getColorFromHex('0b1a3b'),
      ),
    );
  }

  _showConfirmedTimeSheets() {
    return ListView.builder(
      itemCount: unConfirmedTimeSheets.length,
      shrinkWrap: true,
      primary: false,
      itemBuilder: (BuildContext context, index) => Container(
        child: (isLoading)
            ? Text('Loading,,,,')
            : _getTimelistCard(confirmedTimeSheets, index, 'confirmed'),
      ),
    );
  }

  _showUnConfirmedTimeSheets() {
    return ListView.builder(
      itemCount: unConfirmedTimeSheets.length,
      shrinkWrap: true,
      primary: false,
      itemBuilder: (BuildContext context, index) => Container(
        child: (isLoading)
            ? Text('Loading,,,,')
            : _getTimelistCard(unConfirmedTimeSheets, index, 'unConfirmed'),
      ),
    );
  }

  Widget getDate(list, index) {
    var date = Jiffy(list[index]['startDate']).format('dd');
    return Text(date);
  }

  Widget getMonth(list, index) {
    var date = Jiffy(list[index]['startDate']).format('MMM');
    return Text(date);
  }

  Widget _showDate(list, index) {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Column(
        children: [getDate(list, index), getMonth(list, index)],
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: getColorFromHex('f4f4f9'),
      ),
    );
  }

  _getSchoolName(list, index) {
    var _school = list[index]['school'] != 'null' ? list[index]['school'] : {};
    return Text(
      _school['name'],
      textAlign: TextAlign.left,
      style: TextStyle(
          fontSize: 13.0,
          color: getColorFromHex('0b1a3b'),
          fontWeight: FontWeight.bold),
    );
  }

  _getWorkingHours(list, index) {
    return Text(
      "working hours :  ${list[index]['workingHours']}",
      style: TextStyle(fontSize: 12.0, color: getColorFromHex('#bdbdd1')),
    );
  }

  _getTimelistCard(list, index, type) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 19.0),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  _showDate(list, index),
                  SizedBox(width: 10.0),
                  GestureDetector(
                    onTap: () {
                      var _hash = list[index]['hash'];
                      Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => Timesheet(_hash)),
                      );
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        _getSchoolName(list, index),
                        _getWorkingHours(list, index)
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _getBody() {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 10.0,
            ),
            getPageTitle(),
            SizedBox(
              height: 10.0,
            ),
            if (unConfirmedTimeSheets.length > 0) _getUnConfirmedBlock(),
            SizedBox(
              height: 10.0,
            ),
            if (confirmedTimeSheets.length > 0) _getconfirmedBlock(),
            SizedBox(
              height: 10.0,
            ),
            if (unConfirmedTimeSheets.length > 0)
              Text(
                'Unconfirmed Timesheets',
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: 16.0,
                    color: getColorFromHex('0b1a3b'),
                    fontWeight: FontWeight.bold),
              ),
            SizedBox(
              height: 10.0,
            ),
            if (unConfirmedTimeSheets.length > 0) _showUnConfirmedTimeSheets(),
            SizedBox(
              height: 10.0,
            ),
            if (confirmedTimeSheets.length > 0) _showConfirmedTimeSheets(),
            if (unConfirmedTimeSheets.length == 0)
              Text(
                ' You have no unconfirmed timesheets',
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 16.0,
                  color: getColorFromHex('0b1a3b'),
                ),
              ),
            SizedBox(
              height: 10.0,
            ),
            if (confirmedTimeSheets.length == 0)
              Text(
                ' You have no confirmed timesheets',
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 16.0,
                  color: getColorFromHex('0b1a3b'),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
