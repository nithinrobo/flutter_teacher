import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:supplywell_teacher_app/src/screens/booking_list.dart';
import 'package:supplywell_teacher_app/src/screens/dashboard.dart';
import 'package:supplywell_teacher_app/src/screens/edit_bio.dart';
import 'package:supplywell_teacher_app/src/screens/edit_contactDetails.dart';
import 'package:supplywell_teacher_app/src/screens/edit_login.dart';
import 'package:supplywell_teacher_app/src/screens/edit_name.dart';
import 'package:supplywell_teacher_app/src/screens/home.dart';
import 'package:supplywell_teacher_app/src/screens/nav.dart';
import 'package:supplywell_teacher_app/src/screens/notifications.dart';
import 'package:supplywell_teacher_app/src/services/users.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:file_picker/file_picker.dart';

import 'package:supplywell_teacher_app/src/widgets/app_header.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  String _email = '', _mob = '', _firstName = '', _lastName = '', _bio = '';
  bool isLoading = false;
  bool _profileIconClicked = false;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  var _dashboard = {};
  List _todaysBook = [];
  List pages = [Dasboard(), BookingList(), Notifications()];
  final ImagePicker _picker = ImagePicker();
  PickedFile _imageFile = PickedFile('');

  @override
  void initState() {
    init();
    super.initState();
  }

  init() async {
    getUserDashboard();
    getProfileDetails();
  }

  getUserDashboard() async {
    User instance = User();
    isLoading = true;
    var res = await instance.getUserDashboard();
    isLoading = false;
    var data = json.decode(res.body);

    setState(() {
      _dashboard = data;
      if (_dashboard['todaysBooking'].length == 0) {
        _todaysBook = _dashboard['todaysBooking'];
      } else {
        _todaysBook.add(_dashboard['todaysBooking']);
      }
    });
  }

  getProfileDetails() async {
    User instance = User();
    isLoading = true;
    var res = await instance.getUserProfileDetails();
    isLoading = false;
    var data = json.decode(res.body);
    print('body-$data');
    setState(() {
      _mob = data['contact']['mobileNumber'] as String;
      _email = data['contact']['email'] as String;
      _firstName = data['user']['firstName'] as String;
      _lastName = data['user']['lastName'] as String;
      _bio = data['bio'] as String;
    });
  }

  logout() async {
    final SharedPreferences prefs = await _prefs;
    await prefs.clear();

    Navigator.pushReplacement(
      context,
      new MaterialPageRoute(builder: (context) => Home()),
    );
  }

  getTkn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('token');
  }

  //
  uploadImageHTTP(file, url) async {
    User instance = User();
    var res =
        await instance.updateUserProfile({'file': file.readAsByteString()});
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      print('imgData-$data');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(true, true, 1),
      backgroundColor: getColorFromHex('e2e7f0'),
      resizeToAvoidBottomInset: false,
      body: _getBody(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          logout();
        },
        icon: Icon(Icons.logout),
        label: Text("logout"),
        backgroundColor: getColorFromHex('0b1a3b'),
      ),
    );
  }

  _navigate(type) {
    var path;
    if (type == 'contact') {
      path = EditContactDetails();
    } else if (type == 'name') {
      path = EditName();
    } else {
      path = EditBio();
    }
    Navigator.push(context, MaterialPageRoute(builder: (context) => path));
  }

  //
  Widget getLogoutIcon() {
    return GestureDetector(
      onTap: () {
        logout();
      },
      child: Icon(
        Icons.logout,
        size: 30.0,
        color: Colors.white,
      ),
    );
  }

  _getText(text, fontSize) {
    return Text(
      '$text',
      style: TextStyle(
        color: getColorFromHex('0b1a3b'),
        fontWeight: FontWeight.w500,
        fontSize: fontSize,
      ),
    );
  }

  Widget _showProfileIcon() {
    return Container(
      child: _imageFile == null
          ? Icon(
              Icons.account_circle_sharp,
              size: 100.0,
              color: Colors.black54,
            )
          : CircleAvatar(
              radius: 80.0,
              backgroundImage: FileImage(
                File(_imageFile.path),
              ),
            ),
    );
  }

  Widget _showPageHeading() {
    return Text(
      'Profile',
      textAlign: TextAlign.center,
      style: TextStyle(
        color: getColorFromHex('0b1a3b'),
        fontWeight: FontWeight.w100,
        fontFamily: 'Akkurat Pro Regular',
        fontSize: 30.0,
      ),
    );
  }

  Widget getName() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          child: Text(
            'Name:',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 20.0,
              color: getColorFromHex('0b1a3b'),
            ),
          ),
        ),
        Container(
          child: Text(
            '$_firstName  $_lastName',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 15.0,
              color: Colors.grey[500],
            ),
          ),
        ),
      ],
    );
  }

  Widget _showEditBtn() {
    return FlatButton.icon(
      onPressed: () {
        showModalBottomSheet(
          context: context,
          builder: ((builder) => bottomSheet()),
        );
      },
      icon: Icon(
        Icons.edit_outlined,
        size: 15.0,
      ),
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: Colors.black87,
          width: 1,
          style: BorderStyle.solid,
        ),
      ),
      label: Text(
        'change pic',
        style: TextStyle(
          color: getColorFromHex('0b1a3b'),
          fontSize: 15.0,
        ),
      ),
    );
  }

  Widget _showHoriZontalLine(height) {
    return Divider(
      color: getColorFromHex('#595959'),
      height: height,
      thickness: 1.0,
    );
  }

  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  _getBody() {
    if (_todaysBook.length > 0) {
      pages = [Dasboard(), Profile(), Notifications()];
    }
    return Container(
      child: isLoading
          ? Center(child: CircularProgressIndicator())
          : _showProfileDetails(),
    );
  }

  Widget marginTop(height) {
    return SizedBox(height: height);
  }

  void takePhoto(ImageSource source) async {
    var url = 'https://bankapi.nfamilyclub.com/v1/updateProfile';
    final pickedFile = await _picker.getImage(
      source: source,
    );
    setState(() {
      _imageFile = pickedFile;
    });
  }

  Widget bottomSheet() {
    return Container(
      height: 100.0,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      child: Column(
        children: <Widget>[
          Text(
            "Choose Profile photo",
            style: TextStyle(
              fontSize: 20.0,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            FlatButton.icon(
              icon: Icon(Icons.camera),
              onPressed: () {
                takePhoto(ImageSource.camera);
              },
              label: Text("Camera"),
            ),
            FlatButton.icon(
              icon: Icon(Icons.image),
              onPressed: () {
                takePhoto(ImageSource.gallery);
              },
              label: Text("Gallery"),
            ),
          ])
        ],
      ),
    );
  }

  _showSection(section, value) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _sectionTitle(section),
            FlatButton.icon(
              onPressed: () {
                _navigate(section);
              },
              icon: Icon(
                Icons.edit_outlined,
                size: 12.0,
                color: getColorFromHex('fd4f57'),
              ),
              label: Text(
                'Edit',
                style: TextStyle(
                  color: getColorFromHex('fd4f57'),
                  fontSize: 12.0,
                  fontFamily: 'Akkurat Pro Regular',
                ),
              ),
            )
          ],
        ),
        _showHoriZontalLine(1.0),
        marginTop(5.0),
        if (section == 'name' || section == 'bio')
          _SectionValue(section, value)
        else
          _showContactInfo()
      ],
    );
  }

  _showContactInfo() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _getContactInfoData('phone', '$_mob'),
        _getContactInfoData('Email', '$_email')
      ],
    );
  }

  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

  _sectionTitle(section) {
    var title = capitalize(section);
    return Text(
      '$title:',
      style: TextStyle(
        fontSize: 17.0,
        fontWeight: FontWeight.w600,
        fontFamily: 'Akkurat Pro Bold',
        color: getColorFromHex('0b1a3b'),
      ),
    );
  }

  _SectionValue(section, value) {
    var val = (section == 'bio' && value == null) ? 'Please enter bio' : value;
    return Text(
      val,
      style: TextStyle(
        fontFamily: 'Akkurat Pro Regular',
        fontSize: 15.0,
        color: getColorFromHex('595959'),
        fontWeight: FontWeight.w500,
      ),
    );
  }

  Widget _showProfileDetails() {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            marginTop(5.0),
            _showPageHeading(),
            marginTop(5.0),
            _showProfileIcon(),
            marginTop(5.0),
            Container(
              padding: EdgeInsets.fromLTRB(90.0, 0, 90.0, 0),
              child: _showEditBtn(),
            ),
            marginTop(20.0),
            _showSection('name', '$_firstName $_lastName'),
            marginTop(20.0),
            _showSection('bio', '$_bio'),
            marginTop(20.0),
            _showSection('contact', '$_bio'),
            marginTop(20.0),
            marginTop(20.0),
            marginTop(20.0),
          ],
        ),
      ),
    );
  }

  Widget _getContactInfoData(label, value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          child: Text(
            '$label',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontFamily: 'Akkurat Pro Regular',
              fontSize: 15.0,
              color: getColorFromHex('595959'),
            ),
          ),
        ),
        Container(
          child: Text(
            '$value',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 15.0,
              color: getColorFromHex('0b1a3b'),
            ),
          ),
        ),
      ],
    );
  }

  Widget _getUpdateInfo(type) {
    return Container(
      padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: getColorFromHex('#845ff9'),
        borderRadius: BorderRadius.circular(10),
      ),
      child: GestureDetector(
        onTap: () {
          _navigate(type);
        },
        child: Column(
          children: [
            Container(
              child: Text(
                'Edit ${type.toUpperCase()} Details >',
                style: TextStyle(
                  fontSize: 18.0,
                  color: getColorFromHex('#bdbdd1'),
                ),
              ),
            ),
            marginTop(40.0),
            Container(
              child: Text(
                'Update ${type.toUpperCase()} Details',
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
