import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:supplywell_teacher_app/src/screens/timesheet_confirm.dart';
import 'package:supplywell_teacher_app/src/screens/timesheet_contest.dart';
import 'package:supplywell_teacher_app/src/services/timesheets.dart';
import 'package:supplywell_teacher_app/src/widgets/app_header.dart';

class Timesheet extends StatefulWidget {
  String hash;

  Timesheet(this.hash);

  @override
  _TimesheetState createState() => _TimesheetState();
}

class _TimesheetState extends State<Timesheet> {
  var _timeSheetData = {}, _schoolData = {}, _teacher = {};

  bool isLoading = false;
  @override
  void initState() {
    init();
    super.initState();
  }

  init() async {
    isLoading = true;
    TimeSheetService instance = TimeSheetService();
    var res = await instance.getTimeSheet(widget.hash);
    isLoading = false;
    if (res.statusCode == 200) {
      setState(() {
        var data = json.decode(res.body);
        _timeSheetData = data;
        _schoolData =
            _timeSheetData['school'] != 'null' ? _timeSheetData['school'] : {};
        _teacher = _timeSheetData['teacher'] != 'null'
            ? _timeSheetData['teacher']
            : {};
      });
    }
  }

  confirmTimesheet() async {
    TimeSheetService instance = TimeSheetService();
    var res = await instance.confirmTimeSheet(widget.hash);
    if (res.statusCode == 200) {
      onTimesheetConfirmed();
    }
  }

  onConfirmBtnClicked() {
    confirmTimesheet();
  }

  onTimesheetConfirmed() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => TimesheetConfirm()));
  }

  contestTimesheet() {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => TimesheetContest(widget.hash)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getColorFromHex('e2e7f0'),
      appBar: CustomAppBar(true, true, 1),
      body:
          (isLoading) ? Center(child: CircularProgressIndicator()) : _getBody(),
    );
  }

  Widget _getUpdateInfo(title, subtitle, bgColor, textColor, sideHeadingColor) {
    return Container(
      padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: getColorFromHex(bgColor),
        borderRadius: BorderRadius.circular(10),
      ),
      child: GestureDetector(
        onTap: () {},
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
              child: Text(
                '$title',
                style: TextStyle(
                  fontSize: 16.0,
                  color: getColorFromHex(textColor),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              child: Text(
                subtitle,
                style: TextStyle(
                  fontSize: 18.0,
                  color: getColorFromHex(sideHeadingColor),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  Widget getPageTitle() {
    return Text(
      'Timesheet',
      textAlign: TextAlign.left,
      style: TextStyle(
        fontSize: 20.0,
        color: getColorFromHex('0b1a3b'),
      ),
    );
  }

  _getTheBtns() {
    return Column(
      children: [
        SizedBox(
          height: 10.0,
        ),
        Container(
            width: MediaQuery.of(context).size.width,
            child: ElevatedButton(
              child: Text('Confirm timesheet'),
              onPressed: onConfirmBtnClicked,
              style: ElevatedButton.styleFrom(
                primary: getColorFromHex('c0e0cf'), // background
                padding: EdgeInsets.all(20.0),
                elevation: 0.0,
                shadowColor: Colors.transparent,
              ),
            )),
        SizedBox(
          height: 10.0,
        ),
        Container(
            width: MediaQuery.of(context).size.width,
            child: ElevatedButton(
              child: Text('The timesheet is incorrect'),
              onPressed: contestTimesheet,
              style: ElevatedButton.styleFrom(
                primary: getColorFromHex('c0e0cf'), // background
                padding: EdgeInsets.all(20.0),
                elevation: 0.0,
                shadowColor: Colors.transparent,
              ),
            ))
      ],
    );
  }

  _getParagraph() {
    return Text(
      'Please look over the information below and either confirm or contest this timesheet.',
      textAlign: TextAlign.left,
      style: TextStyle(
        fontSize: 14.0,
      ),
    );
  }

  _getBody() {
    return Container(
      padding: EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 8.0),
          getPageTitle(),
          SizedBox(height: 8.0),
          _getParagraph(),
          SizedBox(height: 12.0),
          _getContactInfo(),
          SizedBox(height: 12.0),
          if (!_teacher['confirmed']) _getTheBtns(),
          SizedBox(height: 8.0),
          if (_teacher['confirmed'] == true)
            _getUpdateInfo(
                'TIMESHEET CONFIRMATION',
                'You have confirmed this timesheet',
                'c0e0cf',
                '#ffffff',
                '#ffffff'),
          SizedBox(height: 10.0),
          if (_teacher['confirmed'] == true &&
              _schoolData['confirmed'] == false)
            _getUpdateInfo(
                'TIMESHEET CONFIRMATION',
                "We're still waiting for ${_schoolData['name']} to confirm their timesheet",
                '#ff5f5f',
                '#ffffff',
                '#ffffff'),
          SizedBox(height: 10.0),
          if (_teacher['confirmed'] == true && _schoolData['confirmed'] == true)
            _getUpdateInfo(
                'Timesheet Confirmed',
                "his Timesheet has been confirmed by both parties",
                '#ff5f5f',
                '#ffffff',
                '#ffffff'),
          SizedBox(height: 8.0),
        ],
      ),
    );
  }

  //if teacher and school confirmation is false

  //if both are true

  Widget _getText(text, fontSize, color) {
    return Text(
      '$text',
      style: TextStyle(
        color: getColorFromHex(color),
        fontWeight: FontWeight.w500,
        fontSize: fontSize,
      ),
    );
  }

  Widget _getDot(color) {
    return Container(
      width: 5.0,
      height: 5.0,
      decoration: BoxDecoration(
        color: getColorFromHex(color),
        borderRadius: BorderRadius.circular(12),
      ),
    );
  }

  Widget _getContactInfoData(label, value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          child: Text(
            '$label',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 15.0,
              color: getColorFromHex('0b1a3b'),
            ),
          ),
        ),
        Container(
          child: Text(
            '$value',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 15.0,
              color: Colors.grey[500],
            ),
          ),
        ),
      ],
    );
  }

  Widget _getContactInfo() {
    return Container(
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(12),
        border: Border.all(color: getColorFromHex('eaebf4')),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              _getDot('#ff5f5f'),
              SizedBox(
                width: 10.0,
              ),
              _getText('TIMESHEET', 14.0, '#bdbdd1'),
            ],
          ),
          SizedBox(
            height: 5.0,
          ),
          _getText(_schoolData['name'], 18.0, '#0b1a3b'),
          SizedBox(
            height: 10.0,
          ),
          _getContactInfoData('Start date', _timeSheetData['startDate']),
          _getContactInfoData('End date', _timeSheetData['endDate']),
          _getContactInfoData('Working hours', _timeSheetData['workingHours']),
          _getContactInfoData(
              'Primary Subject', _timeSheetData['primarySubject']),
        ],
      ),
    );
  }
}
