import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:supplywell_teacher_app/src/screens/booking.dart';
import 'package:supplywell_teacher_app/src/screens/calendar.dart';
import 'package:supplywell_teacher_app/src/screens/nav.dart';
import 'package:supplywell_teacher_app/src/screens/notifications.dart';
import 'package:supplywell_teacher_app/src/screens/profile.dart';
import 'package:supplywell_teacher_app/src/services/users.dart';
import 'package:supplywell_teacher_app/src/widgets/custom_font.dart';
import 'package:jiffy/jiffy.dart';

class Dasboard extends StatefulWidget {
  @override
  _DasboardState createState() => _DasboardState();
}

class _DasboardState extends State<Dasboard> {
  var _fName, _dashboard = {};
  List _upcomingBooks = [], _todaysBook = [];
  bool isLoading = false, _hasBookedToday = false;

  @override
  void initState() {
    init();
    super.initState();
  }

  init() {
    getUserDashboard();
    _fName = _getFirstName();
  }

  _getFirstName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _fName = prefs.getString('fName').toString();
    return _fName;
  }

  getUserDashboard() async {
    User instance = User();
    isLoading = true;
    var res = await instance.getUserDashboard();
    var data = json.decode(res.body);
    isLoading = false;

    setState(() {
      _dashboard = data;
      _upcomingBooks = _dashboard['upcomingBookings'];
      if (_dashboard['todaysBooking'].length == 0) {
        _todaysBook = _dashboard['todaysBooking'];
      } else {
        _todaysBook.add(_dashboard['todaysBooking']);
      }
      _hasBookedToday = _todaysBook.length > 0 ? true : false;
    });
  }

  _formatDate(date) {
    return Jiffy(date).format('dd/MM/yyyy');
  }

  _customFontStyle(text, fontFamily, color, textAlignment, fontSize, fontWeight,
      letterSpacing) {
    return CustomFont(text, fontFamily, fontWeight, textAlignment, fontSize,
        color, letterSpacing);
  }

  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  Widget _getContactInfoData(label, value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          child: Text(
            '$label',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 15.0,
              color: getColorFromHex('0b1a3b'),
            ),
          ),
        ),
        Container(
          child: Text(
            '$value',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 15.0,
              color: Colors.grey[500],
            ),
          ),
        ),
      ],
    );
  }

  Widget _showKeyValuePair(label, value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          child: Text(
            '$label',
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontFamily: 'Akkurat Pro Regular',
              fontSize: 15.0,
              color: getColorFromHex('595959'),
            ),
          ),
        ),
        Container(
          child: Text(
            '$value',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 15.0,
              color: getColorFromHex('0b1a3b'),
            ),
          ),
        ),
      ],
    );
  }

  _getBtnInfo() {
    return FlatButton(
      padding: EdgeInsets.all(20.0),
      minWidth: MediaQuery.of(context).size.width,
      onPressed: null,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'More info',
            style: TextStyle(
              color: getColorFromHex('0b1a3b'),
              fontFamily: 'Akkurat Pro Bold',
              fontSize: 20.0,
            ),
          ),
          SvgPicture.asset(
            'assets/arrow-right-small.svg',
            width: 20.0,
            height: 20.0,
          )
        ],
      ),
      textColor: Colors.white,
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: getColorFromHex('0b1a3b'),
          width: 1,
          style: BorderStyle.solid,
        ),
        borderRadius: BorderRadius.circular(4),
      ),
    );
  }

  _getCard(bookingList, index, type) {
    var _subject = bookingList[index]['primarySubject'];
    var _school = bookingList[index]['school'] != null
        ? bookingList[index]['school']
        : {};
    var _startDate = bookingList[index]['startDate'] != null
        ? bookingList[index]['startDate']
        : {};
    var _endDate = bookingList[index]['endDate'] != null
        ? bookingList[index]['endDate']
        : {};
    var mergedDate =
        _formatDate(_startDate['date']) + ' - ' + _formatDate(_endDate['date']);
    return GestureDetector(
      onTap: () {
        var path;
        if (type == 'today') {
          path = Nav(1);
        } else {
          path = Booking(bookingList[index]['publicId']);
        }
        Navigator.push(
          context,
          new MaterialPageRoute(builder: (context) => path),
        );
      },
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 5.0,
          ),
          _showKeyValuePair('Job', 'Educator'),
          SizedBox(
            height: 10.0,
          ),
          _showKeyValuePair('Nursary', _school['name']),
          SizedBox(
            height: 10.0,
          ),
          _showKeyValuePair('Date', mergedDate),
          SizedBox(
            height: 10.0,
          ),
          _getBtnInfo()
        ],
      ),
    );
  }

  _showUpcomingBookingCard() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // _getText('Upcoming Bookings', 20.0),
        ListView.builder(
          itemCount: _upcomingBooks.length,
          shrinkWrap: true,
          primary: false,
          itemBuilder: (BuildContext context, index) => Container(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
            child: (isLoading)
                ? Text('Loading,,,,')
                : _getCard(_upcomingBooks, index, 'upcoming'),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getColorFromHex('e2e7f0'),
      body: (isLoading) ? showLoading() : _getBody(),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     Navigator.pushReplacement(
      //       context,
      //       new MaterialPageRoute(builder: (context) => Calender()),
      //     ); // Add your onPressed code here!
      //   },
      //   child: Icon(Icons.date_range),
      //   backgroundColor: getColorFromHex('0b1a3b'),
      // ),
    );
  }

  showLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  _getPageTitle() {
    return Center(child: mainTitle());
  }

  Widget mainTitle() {
    return Text(
      'Reminders',
      style: TextStyle(
        fontSize: 33.0,
        color: getColorFromHex('0b1a3b'),
        letterSpacing: 1.0,
        fontFamily: 'Akkurat Pro Regular',
        fontWeight: FontWeight.w100,
      ),
    );
  }

  _bookingSection(section) {
    return Column(
      children: [
        _getSectionTitle(section),
        SizedBox(
          height: 8.0,
        ),
        _showHoriZontalLine(),
        SizedBox(
          height: 8.0,
        ),
        _getNoBookingText(section)
      ],
    );
  }

  _getSectionTitle(section) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        _getTitle(section),
        SizedBox(width: 15.0),
        _getRightArrowIcon(),
      ],
    );
  }

  _getTitle(section) {
    var title =
        (section == 'todayBooking') ? "Today's Booking" : 'Upcoming Bookings';
    return _customFontStyle(title, 'Akkurat Pro Bold', '0b1a3b',
        TextAlign.center, 18.0, FontWeight.w500, 0.0);
  }

  _getRightArrowIcon() {
    return Container(
      child: SvgPicture.asset(
        'assets/arrow-right-small.svg',
        height: 15.0,
        width: 20.0,
      ),
    );
  }

  Widget _showHoriZontalLine() {
    return Divider(
      color: Colors.black,
      height: 6.0,
      thickness: 2.0,
    );
  }

  Widget _getNoBookingText(section) {
    var _textToAppend = (section == 'todayBooking')
        ? 'booking listed for today'
        : 'upcoming bookings';
    var commonText = 'You currently have no $_textToAppend';

    if (section == 'todayBooking' && _todaysBook.length > 0) {
      return _showTodaysBookingCard();
    } else if (section == 'upcomingBooking' && _upcomingBooks.length > 0) {
      return _showUpcomingBookingCard();
    } else {
      return Text(
        commonText,
        style: TextStyle(
          color: getColorFromHex('0b1a3b'),
          fontFamily: 'Akkurat Pro Regular',
          fontSize: 18.0,
        ),
      );
    }
  }

  _getBody() {
    return Container(
      padding: EdgeInsets.all(20.0),
      child: Column(
        children: [
          SizedBox(
            height: 5.0,
          ),
          _getPageTitle(),
          SizedBox(
            height: 20.0,
          ),
          _bookingSection('todayBooking'),
          SizedBox(height: 40.0),
          _bookingSection('upcomingBooking')
        ],
      ),
    );
  }

  Widget _noBookingSection(
      title, subtitle, bgColor, textColor, sideHeadingColor) {
    return Container(
      padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: getColorFromHex(bgColor),
        borderRadius: BorderRadius.circular(10),
      ),
      child: GestureDetector(
        onTap: () {},
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
              child: Text(
                '$title',
                style: TextStyle(
                  fontSize: 16.0,
                  color: getColorFromHex(textColor),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              child: Text(
                subtitle,
                style: TextStyle(
                  fontSize: 18.0,
                  color: getColorFromHex(sideHeadingColor),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _showTodaysBookingCard() {
    return Column(
      children: [
        // SizedBox(height: 10.0),
        ListView.builder(
          itemCount: _todaysBook.length,
          shrinkWrap: true,
          primary: false,
          itemBuilder: (BuildContext context, index) => Container(
            child: (isLoading)
                ? Text('Loading,,,,')
                : _getCard(_todaysBook, index, 'today'),
          ),
        )
      ],
    );
  }
}
