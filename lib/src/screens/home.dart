import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:supplywell_teacher_app/main.dart';
import 'package:supplywell_teacher_app/src/screens/forgot_password.dart';
import 'package:supplywell_teacher_app/src/screens/nav.dart';
import 'package:supplywell_teacher_app/src/services/auth.dart';
import 'package:supplywell_teacher_app/src/services/users.dart';
import 'package:supplywell_teacher_app/src/widgets/app_header.dart';
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart';

import 'package:supplywell_teacher_app/src/widgets/custom_font.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  //variables
  bool isLoading = false;
  late String _email, _password;
  final _formKey = GlobalKey<FormState>();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  var _url;
  var _controller;
  var _alarmTime;
  var _animation;
  Future<void>? _launched;
  var _devTkn = '';

  FocusNode _focusNode = FocusNode();

  @override
  void initState() {
    _alarmTime = DateTime.now();

    super.initState();

    String url = 'https://bankapi.nfamilyclub.com/v1';

    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _animation = Tween(begin: 300.0, end: 50.0).animate(_controller)
      ..addListener(() {
        setState(() {});
      });

    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        _controller.forward();
      } else {
        _controller.reverse();
      }
    });

    DateTime scheduleAlarmDateTime;
    if (_alarmTime.isAfter(DateTime.now()))
      scheduleAlarmDateTime = _alarmTime;
    else
      scheduleAlarmDateTime = _alarmTime.add(Duration(days: 1));

    var alarmInfo = {'title ': 'Supplywell Notif'};
  }

  @override
  void dispose() {
    _controller.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  login() async {
    Auth instance = Auth();
    var response, parsedJson;
    Map params = {'email': _email, 'password': _password};
    response = await instance.login(params);
    parsedJson = json.decode(response.body);
    if (parsedJson['error'] != null) {
      showFlutterToaster(parsedJson['error']);
      return false;
    } else {
      proceed(parsedJson);
      print('parsedJSon-$parsedJson');
    }
  }

  storeDataLocally(details) async {
    final SharedPreferences prefs = await _prefs;
    var user = details['user'];
    String fullName = user['firstName'] + ' ' + user['lastName'];

    setState(() {
      prefs.setString('token', details['token']);
      prefs.setString('name', fullName);
      prefs.setString('fName', user['firstName']);
    });
  }

  proceed(details) async {
    var messaging = FirebaseMessaging.instance;
    User instance = User();

    storeDataLocally(details);

    var _tkn = await messaging.getToken();

    var p = {'userToken': _tkn};
    var res = await instance.createDeviceToken(p);
    print('resStatus-${res.statusCode}');
    print('methor&url-${res.request}');
    print('body-${res.body}');

    //navigate to booking list
    Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => Nav(0)),
    );
  }

  //dart
  onBtnClick() {
    //if the fields are not valid then just throw message
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      login();
    }
  }

  oauthUrl(service) {
    var url = "$_url/oauth/$service";
    print("url-$url");
    return "$_url/oauth/$service";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getColorFromHex('e2e7f0'),
      resizeToAvoidBottomInset: true,
      appBar: CustomAppBar(false, false, 1),
      body: _getBody(),
    );
  }

  //helper widgets

  _customFontStyle(text, fontFamily, color, textAlignment, fontSize, fontWeight,
      letterSpacing) {
    return CustomFont(text, fontFamily, fontWeight, textAlignment, fontSize,
        color, letterSpacing);
  }

  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  showFlutterToaster(msg) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.TOP,
      timeInSecForIosWeb: 3,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  Widget showLoader() {
    return Center(child: Text('Loading'));
  }

  Widget _getBody() {
    return new InkWell(
      splashColor: Colors.transparent,
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: SingleChildScrollView(
        child: Column(
          children: [
            bannerImage(),
            SizedBox(height: 25.0),
            mainTitle(),
            SizedBox(height: 15.0),
            subTitle(),
            SizedBox(height: 10.0),
            // Padding(
            //   padding: EdgeInsets.all(15.0),
            //   child: _getGoogleBtn(),
            // ),
            Padding(
              padding: EdgeInsets.all(5.0),
              child: Text(
                'OR',
                style: TextStyle(color: getColorFromHex('0b1a3b')),
              ),
            ),
            Container(
              padding: EdgeInsets.all(15.0),
              child: getFormFields(),
            ),
          ],
        ),
      ),
    );
  }

  Widget bannerImage() {
    return Container(
      child: AspectRatio(
        aspectRatio: 1.5,
        child: Image.asset(
          'assets/nfamily_hero.jpg',
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget mainTitle() {
    return Text(
      'Welcome',
      style: TextStyle(
        fontSize: 33.0,
        color: getColorFromHex('0b1a3b'),
        letterSpacing: 1.0,
        fontFamily: 'Akkurat Pro Regular',
      ),
    );
  }

  Widget subTitle() {
    var _subHeading = 'Lets find you a shift\n at N Family';
    return _customFontStyle(_subHeading, 'Akkurat Pro Regular', '#0b1a3b',
        TextAlign.center, 21.0, FontWeight.w500, 1.0);
  }

  Future<void> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
      );
    } else {
      throw 'Could not launch $url';
    }
  }

  onGoogleClick() {
    setState(() {
      var url = oauthUrl('google');
      _launched = _launchInBrowser(url);
    });
  }

  Widget _getGoogleBtn() {
    // return RichText(
    //   text: TextSpan(
    //     children: [
    //       TextSpan(
    //         style: TextStyle(color: getColorFromHex('ffffff')),
    //         text: 'Login with google',
    //       ),
    //       TextSpan(
    //         recognizer: TapGestureRecognizer()
    //           ..onTap = () async {
    //             final url = oauthUrl('google');
    //             print('url-$url');
    //             if (await canLaunch(url)) {
    //               await launch(url, forceSafariVC: true, forceWebView: true);
    //             }
    //           },
    //       ),
    //       // TextSpan(
    //       // style: bodyTextStyle,
    //       // text: seeSourceSecond,
    //       // ),
    //     ],
    //   ),
    // );

    return Container(
      width: MediaQuery.of(context).size.width,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: getColorFromHex('#db4437'), // background
          padding: EdgeInsets.all(20.0),
          elevation: 0.0,
          shadowColor: Colors.transparent,
        ),
        child: _getBtnTitle('Login with google', 'ffffff'),
        onPressed: () {
          onGoogleClick();
        },
      ),
    );
    // return GestureDetector(
    //   onTap: () {
    //     onGoogleClick();
    //   },
    //   child: Container(
    //     width: MediaQuery.of(context).size.width,
    //     padding: EdgeInsets.all(15.0),
    //     decoration: BoxDecoration(
    //         color: getColorFromHex('#db4437'),
    //         border: Border.all(color: getColorFromHex('#db4437')),
    //         borderRadius: BorderRadius.all(Radius.circular(10))),
    //     child: _getBtnTitle('Login with google', 'ffffff'),
    //   ),
    // );
  }

  navigate() {
    Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => ForgotPassword()),
    );
  }

  Widget forgotPassword() {
    return GestureDetector(
      onTap: navigate,
      child: _customFontStyle(
          'Forgotten your password? Click to reset',
          'Akkurat Pro Bold',
          '0b1a3b',
          TextAlign.center,
          16.0,
          FontWeight.bold,
          null),
    );
  }

  Widget getFormFields() {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          _emailField(),
          SizedBox(height: 10.0),
          _passwordField(),
          SizedBox(height: 10.0),
          _submitBtn(),
          SizedBox(height: 20.0),
          forgotPassword()
        ],
      ),
    );
  }

  InputDecoration _formFieldDecoration(fieldName) {
    return InputDecoration(
      labelText: fieldName,
      filled: true,
      fillColor: getColorFromHex('#ffffff'),
      border: OutlineInputBorder(),
    );
  }

  Widget _emailField() {
    return TextFormField(
      decoration: _formFieldDecoration('Email Address'),
      focusNode: _focusNode,
      validator: (String? value) {
        if (value != null && value.isEmpty) {
          return 'Email is Required';
        }

        if (value != null &&
            !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                .hasMatch(value)) {
          return 'Please enter a valid email Address';
        }

        return null;
      },
      onSaved: (String? value) {
        if (value != null) _email = value;
      },
    );
  }

  Widget _passwordField() {
    return TextFormField(
      decoration: _formFieldDecoration('Password'),
      obscureText: true,
      keyboardType: TextInputType.visiblePassword,
      validator: (String? value) {
        if (value != null && value.isEmpty) {
          return 'Password is Required';
        }
        return null;
      },
      onSaved: (String? value) {
        if (value != null) _password = value;
      },
    );
  }

  Widget _submitBtn() {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: getColorFromHex('c0e0cf'), // background
        padding: EdgeInsets.all(20.0),
        elevation: 0.0,
        shadowColor: Colors.transparent,
      ),
      child: _getBtnTitle('Submit', '000000'),
      onPressed: onBtnClick,
    );
  }

  Widget _getBtnTitle(title, color) {
    return _customFontStyle(title, 'Akkurat Pro Regular', color,
        TextAlign.center, 20.0, FontWeight.w400, null);
  }
}
