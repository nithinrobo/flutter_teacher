import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:supplywell_teacher_app/src/screens/nav.dart';
import 'package:supplywell_teacher_app/src/screens/profile.dart';
import 'package:supplywell_teacher_app/src/services/users.dart';
import 'package:supplywell_teacher_app/src/widgets/app_header.dart';

class EditContactDetails extends StatefulWidget {
  @override
  _EditContactDetailsState createState() => _EditContactDetailsState();
}

class _EditContactDetailsState extends State<EditContactDetails> {
  bool isLoading = false;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  var _email = '', _mob = '', _firstName = '', _lastName = '';

  TextEditingController _firstNameCtrl = new TextEditingController();
  TextEditingController _lastNameCtrl = new TextEditingController();
  TextEditingController _phone = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    getProfileDetails();
  }

  getProfileDetails() async {
    User instance = User();
    isLoading = true;
    var res = await instance.getUserProfileDetails();
    isLoading = false;
    Map data = json.decode(res.body);
    setState(() {
      _mob = data['contact']['mobileNumber'] as String;
      _email = data['contact']['email'] as String;
      _firstName = data['user']['firstName'] as String;
      _lastName = data['user']['lastName'] as String;
    });
    _firstNameCtrl.text = _firstName;
    _lastNameCtrl.text = _lastName;
    _phone.text = _mob;
    _emailController.text = _email;
  }

  navigateToProfile() {
    Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => Profile()),
    );
  }

  navigateNotification() {
    Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => Nav(3)),
    );
  }

  void submit(params) async {
    User instance = User();
    var convertedJson = jsonEncode(params);
    var res = await instance.updateUserProfile(convertedJson);
    print(res.statusCode);
    if (res.statusCode == 200) {
      navigateToProfile();
    }
  }

  Widget _getBackBtn() {
    return GestureDetector(
      onTap: () {
        navigateToProfile();
      },
      child: Icon(
        Icons.arrow_left_outlined,
        size: 30.0,
        color: Colors.white,
      ),
    );
  }

  Widget getProfileIcon() {
    return GestureDetector(
      onTap: () {
        navigateToProfile();
      },
      child: Icon(
        Icons.account_circle_sharp,
        size: 30.0,
        color: Colors.white,
      ),
    );
  }

  Widget getTitle() {
    return Text(
      'n.',
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Colors.white,
        fontSize: 30.0,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  Widget getNotificationIcon() {
    return GestureDetector(
      onTap: () {
        navigateNotification();
      },
      child: Icon(
        Icons.circle_notifications,
        size: 30.0,
        color: Colors.white,
      ),
    );
  }

  PreferredSizeWidget _header() {
    return PreferredSize(
      child: SafeArea(
        child: Container(
          child: Padding(
            padding: EdgeInsets.all(20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                _getBackBtn(),
                getProfileIcon(),
                getTitle(),
                getNotificationIcon()
              ],
            ),
          ),
        ),
      ),
      preferredSize: Size.fromHeight(100),
    );
  }

  showFlutterToaster(msg) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.TOP,
      timeInSecForIosWeb: 3,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget _getSubHeadingText() {
      return Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(bottom: 30.0),
        child: Text(
          'Edit Contact Details',
          style: TextStyle(
            color: getColorFromHex('#0b1a3b'),
            fontWeight: FontWeight.bold,
            fontSize: 20.0,
          ),
        ),
      );
    }

    Widget _firstNameField() {
      return TextField(
          controller: _firstNameCtrl,
          autocorrect: false,
          decoration: InputDecoration(
            labelText: 'Your first name',
            border: OutlineInputBorder(),
          ),
          onChanged: (String nameVal) {
            _firstName = nameVal;
          });
    }

    Widget _lastNameField() {
      return TextField(
        controller: _lastNameCtrl,
        autocorrect: false,
        decoration: InputDecoration(
          labelText: 'Your Last name',
          border: OutlineInputBorder(),
        ),
        onChanged: (String nameVal) {
          _lastName = nameVal;
        },
      );
    }

    Widget _phoneField() {
      return TextField(
          controller: _phone,
          autocorrect: false,
          decoration: InputDecoration(
            labelText: 'Telephone no.',
            border: OutlineInputBorder(),
          ),
          onChanged: (String nameVal) {
            _mob = nameVal;
          });
    }

    Widget _emailField() {
      return TextField(
        controller: _emailController,
        autocorrect: false,
        decoration: InputDecoration(
          labelText: 'Your email address',
          border: OutlineInputBorder(),
        ),
        onChanged: (String nameVal) {
          _email = nameVal;
        },
      );
    }

    Widget _getFormFields() {
      return Container(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              // _firstNameField(),
              // SizedBox(height: 30.0),
              // _lastNameField(),
              // SizedBox(height: 30.0),
              _phoneField(),
              SizedBox(height: 30.0),
              _emailField(),
              SizedBox(height: 30.0),
              Container(
                width: MediaQuery.of(context).size.width,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: getColorFromHex('#51dbf2'), // background
                    padding: EdgeInsets.all(20.0),
                  ),
                  child: Text(
                    'Submit',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onPressed: () {
                    if (!_formKey.currentState!.validate()) {
                      return;
                    }
                    _formKey.currentState!.save();

                    if (_mob == '' || _mob == null) {
                      showFlutterToaster('Please enter telephone no');
                    } else if (_email == '' || _email == null) {
                      showFlutterToaster('Please enter email');
                    } else {
                      var contact = {
                        'mobileNumber': _mob,
                        'email': _email,
                      };

                      var data = {'contact': contact};

                      var params = data;

                      print('data - $data');
                      submit(params);
                    }

                    //Send to API
                  },
                ),
              )
            ],
          ),
        ),
      );
    }

    _getBody() {
      return Container(
        color: getColorFromHex('e2e7f0'),
        padding: EdgeInsets.fromLTRB(45.0, 30.0, 30.0, 30.0),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: isLoading
            ? Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [_getSubHeadingText(), _getFormFields()],
                ),
              ),
      );
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: getColorFromHex('e2e7f0'),
      appBar: CustomAppBar(true, true, 1),
      body: _getBody(),
    );
  }

  /// Convert a color hex-string to a Color object.
  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }
}
