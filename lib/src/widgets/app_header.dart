import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:supplywell_teacher_app/src/screens/nav.dart';
import 'package:supplywell_teacher_app/src/screens/notifications.dart';
import 'package:supplywell_teacher_app/src/screens/profile.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  final bool shouldShowIcons;
  final bool shouldShowBackBtn;
  final int index;
  const CustomAppBar(this.shouldShowIcons, this.shouldShowBackBtn, this.index);

  @override
  Size get preferredSize => Size.fromHeight(100); // default is 56.0

  @override
  _CustomAppBarState createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  goBack() {
    Navigator.pop(context);
  }

  navigateToProfile() {
    Navigator.pushReplacement(
      context,
      new MaterialPageRoute(builder: (context) => Profile()),
    );
  }

  navigateToNotification() {
    Navigator.pushReplacement(
      context,
      new MaterialPageRoute(builder: (context) => Notifications()),
    );
  }

  Widget getProfileIcon() {
    return GestureDetector(
      onTap: () {
        navigateToProfile();
      },
      child: Icon(
        Icons.account_circle_sharp,
        size: 30.0,
        color: Colors.white,
      ),
    );
  }

  Widget getNotificationIcon() {
    return GestureDetector(
      onTap: () {
        navigateToNotification();
      },
      child: Icon(
        Icons.circle_notifications,
        size: 30.0,
        color: Colors.white,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      child: SafeArea(
        child: Container(
          color: getColorFromHex('0b1a3b'),
          child: Padding(
            padding: EdgeInsets.all(15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                if (widget.shouldShowBackBtn && widget.index != 0)
                  _showBackBtn(),
                if (widget.shouldShowIcons)
                  getProfileIcon()
                else
                  SizedBox(width: 5.0),
                Container(
                  child: SvgPicture.asset(
                    'assets/nfamily_logo_01.svg',
                    height: 20.0,
                    width: 20.0,
                  ),
                ),
                if (widget.shouldShowBackBtn && widget.index != 0)
                  SizedBox(width: 3.0),
                if (widget.shouldShowIcons)
                  getNotificationIcon()
                else
                  SizedBox(width: 5.0),
              ],
            ),
          ),
        ),
      ),
      preferredSize: Size.fromHeight(100),
    );
  }

  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  Widget _showBackBtn() {
    return GestureDetector(
      onTap: goBack,
      child: Icon(
        Icons.arrow_back_ios_new_outlined,
        size: 20.0,
        color: Colors.white,
      ),
    );
  }
}
