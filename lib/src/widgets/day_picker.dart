import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
export 'package:flutter_date_pickers/src/day_picker.dart';
import 'package:flutter_date_pickers/flutter_date_pickers.dart' as dp;
import 'package:flutter_date_pickers/flutter_date_pickers.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:supplywell_teacher_app/src/services/booking.dart';
import 'package:supplywell_teacher_app/src/services/users.dart';
import 'package:supplywell_teacher_app/src/widgets/color_btn_dialog.dart';
import 'package:jiffy/jiffy.dart';
import 'event.dart';

class DayPickerPage extends StatefulWidget {
  @override
  _DayPickerPageState createState() => _DayPickerPageState();
}

class _DayPickerPageState extends State<DayPickerPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  DateTime _selectedDate = DateTime.now();
  DateTime _firstDate = DateTime.now().subtract(Duration(days: 1));
  DateTime _lastDate = DateTime.now().add(Duration(days: 13));

  Color selectedDateStyleColor = Colors.blue;
  Color selectedSingleDateDecorationColor = Colors.red;

  bool isLoading = false;

  var _thisDay,
      _thisMonth,
      _thisYear,
      _startTime,
      _endTime,
      _calendarList = [],
      _calendar = [],
      timeList = [
        "06:00",
        "06:30",
        "07:00",
        "07:30",
        "08:00",
        "08:30",
        "09:00",
        "09:30",
        "10:00",
        "10:30",
        "11:00",
        "11:30",
        "12:00",
        "12:30",
        "13:00",
        "13:30",
        "14:00",
        "14:30",
        "15:00",
        "15:30",
        "16:00",
        "16:30",
        "17:00",
        "17:30",
        "18:00"
      ],
      _availableDaysList = [];

  List<Event> events = [];

  @override
  void initState() {
    DateTime now = new DateTime.now();
    _thisDay = now.day;
    _thisMonth = now.month;
    _thisYear = now.year;
    getAvailability();
    super.initState();
  }

  loadCalendar(date) {
    getCalendar(date);
    getCalendarList(date);
  }

  getCalendarList(date) async {
    User instance = User();
    var res = await instance.getUserCalendarList(date);
    if (res.statusCode == 200) {
      setState(() {
        _calendarList = json.decode(res.body);
        print('calen-list - $_calendarList');
      });
    }
  }

  getCalendar(date) async {
    User instance = User();
    var res = await instance.getUserCalendar(date);
    if (res.statusCode == 200) {
      setState(() {
        _calendar = json.decode(res.body);
        print('calen - $_calendar');
      });
    }
  }

  getAvailability() async {
    var mappedRes;
    Bookings instance = Bookings();
    var res = await instance.getAvailablity();
    if (res.statusCode == 200) {
      setState(() {
        _availableDaysList = json.decode(res.body);
        mappedRes = _availableDaysList
            .map((ele) => DateTime.parse(ele['date']))
            .toList();
        events = [];
        for (int i = 0; i < mappedRes.length; i++) {
          events.add(Event(mappedRes[i], 'Ev${i + 1}'));
        }
        print('events - $events');
      });
    }
  }

  getSelectedDate(date) {
    return Jiffy(date).format("EEEE do");
  }

  getDayName(date) {
    return Jiffy(date).format("EEEE");
  }

  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  bookAsAvailable(data, type) {
    // if the user choose the hrs option, then validate start and end time
    if (type == "hrs") {
      var msg = validate();
      if (msg != '') {
        showFlutterToaster(msg, 'error');
        return false;
      }
      Navigator.of(context).pop();
    }
    _dismissDialog();
    this.createAvailable(data, type);
  }

  showFlutterToaster(msg, type) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.TOP,
      timeInSecForIosWeb: 3,
      backgroundColor: (type == 'success') ? Colors.green : Colors.red,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  createAvailable(data, type) async {
    var params = {
      'date': data['date'],
      'fullDay': type == "hrs" ? '0' : '1',
      'startTime': type == "fullDay" ? "09:00" : _startTime,
      'endTime': type == "fullDay" ? "17:00" : _endTime
    };

    Bookings instance = Bookings();
    var res = await instance.createAvailablity(params);
    if (res.statusCode == 200) {
      getAvailability();
      var msg = "";
      if (type == "hrs") {
        msg =
            '${data['date']} has made as available day for $_startTime to $_endTime';
      } else {
        msg = '${data['date']} has made as  available day for full day';
      }
      showFlutterToaster(msg, 'success');
    }
  }

  onAgreed(index) {
    deleteAvailableDay(index);
  }

  deleteAvailableDay(index) async {
    _dismissDialog();
    Bookings instance = Bookings();
    var id = _availableDaysList[index]['id'];
    var params = {'id': id};
    var res = await instance.deleteAvailablity(params);
    if (res.statusCode == 200) {
      getAvailability();
    }
  }

  Widget _getAvailableDays(context) {
    return ListView.builder(
      itemCount: _availableDaysList.length,
      shrinkWrap: true,
      primary: false,
      itemBuilder: (BuildContext context, index) => Container(
        child: (isLoading) ? Text('Loading,,,,') : _getCard(context, index),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    dp.DatePickerRangeStyles styles = dp.DatePickerRangeStyles(
      selectedDateStyle: Theme.of(context)
          .accentTextTheme
          .bodyText1
          ?.copyWith(color: getColorFromHex('000000')),
      selectedSingleDateDecoration: BoxDecoration(
          color: getColorFromHex('c0e0cf'), shape: BoxShape.circle),
      dayHeaderStyle: DayHeaderStyle(
        textStyle: TextStyle(color: Colors.red),
      ),
    );

    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          SizedBox(height: 15.0),
          Padding(
            child: Text(
              'Please select the days you are available for work on the calendar',
              style: TextStyle(
                  fontSize: 16.0,
                  color: getColorFromHex('0b1a3b'),
                  fontWeight: FontWeight.bold),
            ),
            padding: EdgeInsets.fromLTRB(8.0, 0, 0, 0),
          ),
          SizedBox(height: 15.0),
          dp.DayPicker.single(
            selectedDate: _selectedDate,
            onChanged: _onSelectedDateChanged,
            firstDate: _firstDate,
            lastDate: _lastDate,
            datePickerStyles: styles,
            datePickerLayoutSettings: dp.DatePickerLayoutSettings(
              showPrevMonthEnd: true,
              showNextMonthStart: true,
            ),
            selectableDayPredicate: _isSelectableCustom,
            eventDecorationBuilder: _eventDecorationBuilder,
          ),
          SizedBox(
            height: 15.0,
          ),
          Padding(
            child: Text(
              'Available days',
              style: TextStyle(
                  fontSize: 16.0,
                  color: getColorFromHex('0b1a3b'),
                  fontWeight: FontWeight.bold),
            ),
            padding: EdgeInsets.fromLTRB(8.0, 0, 0, 0),
          ),
          SizedBox(
            height: 8.0,
          ),
          _getAvailableDays(context)
        ],
      ),
    );
  }

  markAsUnAvailable(index) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Confirm'),
            content: Text(
                'Are you sure you want to remove this from available days?'),
            actions: <Widget>[
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: getColorFromHex('db4437'), // background
                  padding: EdgeInsets.all(5.0),
                  elevation: 0.0,
                  shadowColor: Colors.transparent,
                ),
                child: Text('Cancel'),
                onPressed: _dismissDialog,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: getColorFromHex('c0e0cf'), // background
                  padding: EdgeInsets.all(5.0),
                  elevation: 0.0,
                  shadowColor: Colors.transparent,
                ),
                child: Text('Ok'),
                onPressed: () {
                  onAgreed(index);
                },
              ),
              // El(
              //     onPressed: () {
              //       _dismissDialog();
              //     },
              //     child: Text('Cancel')),
              // TextButton(
              //   onPressed: () {
              //     onAgreed(index);
              //   },
              //   child: Text('Ok'),
              // )
            ],
          );
        });
  }

  Widget _getUpdateInfo(title, subtitle, type) {
    return Container(
      padding: EdgeInsets.fromLTRB(10.0, 13.0, 0.0, 10.0),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: getColorFromHex('#845ff9'),
        borderRadius: BorderRadius.circular(10),
      ),
      child: GestureDetector(
        onTap: () {
          // _navigate(type);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Text(
                title,
                style: TextStyle(
                  fontSize: 18.0,
                  color: getColorFromHex('#bdbdd1'),
                ),
              ),
            ),
            SizedBox(height: 10.0),
            Container(
              child: Text(
                subtitle,
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _showTypicalNonWorkingDayBlock(index) {
    var title = 'NON-WORKING DAY';
    var subTitle =
        "You've previously told us that Tuesday is typically a day that you do not work. Click here to update your typical working days.";
    return _getUpdateInfo(title, subTitle, 'wholeDayNotWorking');
  }

  _showSelectNonWorkingBlock(index) {}

  Widget getDate(index) {
    var date = Jiffy(_availableDaysList[index]['date']).format('dd');
    return Text(date);
  }

  Widget getMonth(index) {
    var date = Jiffy(_availableDaysList[index]['date']).format('MMM');
    return Text(date);
  }

  Widget _showDate(index) {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Column(
        children: [getDate(index), getMonth(index)],
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: getColorFromHex('f4f4f9'),
      ),
    );
  }

  Widget _getCard(context, index) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: 19.0),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                _showDate(index),
                SizedBox(width: 10.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 8.0,
                    ),
                    Text(
                      'Available',
                      style: TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                        color: getColorFromHex('0b1a3b'),
                      ),
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Text(
                      _availableDaysList[index]['fullDay'] == true
                          ? 'Full Day Availability'
                          : 'Hours ${_availableDaysList[index]['startTime']} - ${_availableDaysList[index]['endTime']}',
                      style: TextStyle(
                        fontSize: 13.0,
                        color: Colors.grey,
                      ),
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    // GestureDetector(
                    //   onTap: () {
                    //     markAsUnAvailable(index);
                    //   },
                    //   child: Text(
                    //     'Remove',
                    //     style: TextStyle(
                    //       fontSize: 12.0,
                    //       fontWeight: FontWeight.bold,
                    //       color: Colors.red,
                    //       decoration: TextDecoration.underline,
                    //     ),
                    //   ),
                    // ),
                  ],
                ),
                GestureDetector(
                  onTap: () {
                    markAsUnAvailable(index);
                  },
                  child: Text(
                    'X',
                    style: TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.red,
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  // select text color of the selected date
  void _showSelectedDateDialog() async {
    Color? newSelectedColor = await showDialog(
        context: context,
        builder: (_) => ColorPickerDialog(
              selectedColor: selectedDateStyleColor,
            ));

    if (newSelectedColor != null) {
      setState(() {
        selectedDateStyleColor = newSelectedColor;
      });
    }
  }

  // select background color of the selected date
  void _showSelectedBackgroundColorDialog() async {
    Color? newSelectedColor = await showDialog(
        context: context,
        builder: (_) => ColorPickerDialog(
              selectedColor: selectedSingleDateDecorationColor,
            ));

    if (newSelectedColor != null) {
      setState(() {
        selectedSingleDateDecorationColor = newSelectedColor;
      });
    }
  }

  _onSelectedDateChanged(DateTime newDate) {
    setState(() {
      print('newDate-$newDate');
      _selectedDate = newDate;
      var convertedDate = Jiffy(_selectedDate).format('yyyy-MM-dd');
      print('convertedDate-$convertedDate');
      var res =
          _availableDaysList.where((i) => i['date'] == convertedDate).toList();
      if (res.length > 0) {
        showFlutterToaster(
            'You have aleady added this date as available day', 'error');
        return;
      }
      print('res-$res');
      onConfirm();
    });
  }

  // ignore: prefer_expression_function_bodies
  bool _isSelectableCustom(DateTime day) {
    return day.weekday < 6;
  }

  dp.EventDecoration? _eventDecorationBuilder(DateTime date) {
    List<DateTime> eventsDates =
        events.map<DateTime>((Event e) => e.date).toList();
    print('eventsDecBuildDats-$eventsDates');

    bool isEventDate = eventsDates.any((DateTime d) =>
        date.year == d.year && date.month == d.month && d.day == date.day);

    BoxDecoration roundedBorder = BoxDecoration(
        border: Border.all(
          color: Colors.deepOrange,
        ),
        borderRadius: BorderRadius.all(Radius.circular(3.0)));

    return isEventDate
        ? dp.EventDecoration(boxDecoration: roundedBorder)
        : null;
  }

  _dismissDialog() {
    Navigator.pop(context);
  }

  onFullDayClicked() {
    var convertedDate = Jiffy(_selectedDate).format('yyyy-MM-dd');
    print('convertedDate - $convertedDate');
    var fiteredRes =
        _availableDaysList.where((element) => element['date'] == convertedDate);
    print('fiteredRes - $fiteredRes');

    var data = {'date': convertedDate};
    bookAsAvailable(data, 'fullDay');
  }

  onHoursClicked() {
    showDialogFunc(context);
  }

  onConfirm() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Confirm'),
            content: Text(
                'Would you like to set your availability for the full day or between certain hours?'),
            actions: <Widget>[
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: getColorFromHex('db4437'), // background
                  padding: EdgeInsets.all(10.0),
                  elevation: 0.0,
                  shadowColor: Colors.transparent,
                ),
                child: Text('By the Hour'),
                onPressed: onHoursClicked,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: getColorFromHex('c0e0cf'), // background
                  padding: EdgeInsets.all(10.0),
                  elevation: 0.0,
                  shadowColor: Colors.transparent,
                ),
                child: Text(
                  'Full Day',
                  style: TextStyle(color: getColorFromHex('000000')),
                ),
                onPressed: onFullDayClicked,
              ),

              // TextButton(onPressed: onHoursClicked, child: Text('By the Hour')),
              // TextButton(
              //   onPressed: onFullDayClicked,
              //   child: Text('Full Day'),
              //   style: ButtonStyle(
              //     backgroundColor:
              //         MaterialStateProperty.all(Theme.of(context).accentColor),
              //   ),
              // )
            ],
          );
        });
  }

  onCancel() {
    _dismissDialog();
  }

  Widget _getStartTimeField() {
    return StatefulBuilder(
        builder: (BuildContext context, StateSetter dropDownState) {
      return new DropdownButton(
        value: _startTime,
        hint: Text('Pleas select Start time'),
        isExpanded: true,
        items: timeList.map((value) {
          return new DropdownMenuItem(
            value: value,
            child: new Text(value),
          );
        }).toList(),
        onChanged: (changedValue) {
          dropDownState(() {
            print('changed - ${changedValue.toString()}');
            _startTime = changedValue;
          });
        },
      );
    });
  }

  Widget _getEndTimeField() {
    return StatefulBuilder(
        builder: (BuildContext context, StateSetter dropDownState) {
      return new DropdownButton(
        value: _endTime,
        hint: Text('Pleas select End time'),
        isExpanded: true,
        items: timeList.map((value) {
          return new DropdownMenuItem(
            value: value,
            child: new Text(value),
          );
        }).toList(),
        onChanged: (changedValue) {
          dropDownState(() {
            print('changed - ${changedValue.toString()}');
            _endTime = changedValue;
          });
        },
      );
    });
  }

  validate() {
    var msg = '';
    if (_startTime == null) {
      msg = 'Please select Start time';
    } else if (_endTime == null) {
      msg = 'Please select End time';
    }

    return msg;
  }

  onSubmit() {
    var convertedDate = Jiffy(_selectedDate).format('yyyy-MM-dd');
    print('convertedDate - $convertedDate');
    var fiteredRes =
        _availableDaysList.where((element) => element['date'] == convertedDate);
    print('fiteredRes - $fiteredRes');

    var data = {'date': convertedDate};
    bookAsAvailable(data, 'hrs');
  }

  showDialogFunc(context) {
    return showDialog(
      context: context,
      builder: (context) {
        return Padding(
          padding: const EdgeInsets.all(20.0),
          child: Center(
            child: Material(
              shadowColor: getColorFromHex('0b1a3b'),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                ),
                padding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      _getStartTimeField(),
                      SizedBox(height: 10.0),
                      _getEndTimeField(),
                      RaisedButton(
                        padding: EdgeInsets.all(20.0),
                        color: getColorFromHex('#c0e0cf'),
                        child: Text(
                          'Submit',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        onPressed: onSubmit,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
