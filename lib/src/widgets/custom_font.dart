import 'package:flutter/material.dart';

class CustomFont extends StatefulWidget {
  final String text;
  final FontWeight fontWeight;
  final TextAlign textAlign;
  final double fontSize;
  final String color;
  final String fontFamily;
  final double letterSpacing;

  CustomFont(this.text, this.fontFamily, this.fontWeight, this.textAlign,
      this.fontSize, this.color, this.letterSpacing);

  @override
  _CustomFontState createState() => _CustomFontState();
}

class _CustomFontState extends State<CustomFont> {
  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  @override
  Widget build(BuildContext context) {
    return Text(
      widget.text,
      textAlign: widget.textAlign,
      style: TextStyle(
        fontWeight: widget.fontWeight != null ? widget.fontWeight : null,
        fontSize: widget.fontSize,
        letterSpacing:
            widget.letterSpacing != null ? widget.letterSpacing : null,
        fontFamily: widget.fontFamily,
        color: getColorFromHex(widget.color),
      ),
    );
  }
}
