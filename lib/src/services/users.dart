import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';

class User {
  String url = 'https://bankapi.nfamilyclub.com/v1';

  getTkn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('token');
  }

  getHeader() async {
    return {'Authorization': 'Bearer ' + await this.getTkn()};
  }

  Future<http.Response> createUserAbsence(params) async {
    return http.post('$url/createAbsence',
        headers: <String, String>{
          'Authorization': 'Bearer ' + await this.getTkn(),
        },
        body: json.encode(params));
  }

  Future<http.Response> updateUserProfile(params) async {
    var headers, postData;

    headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      HttpHeaders.authorizationHeader: 'Bearer ' + await this.getTkn(),
    };

    postData = params.toString();

    print('params-${params['file']}');

    if (params['file'] != null) {
      postData = params['file'];
      headers = {
        HttpHeaders.authorizationHeader: 'Bearer ' + await this.getTkn(),
        'Content-Type': 'multipart/form-data'
      };
    }

    return http.post('$url/updateProfile', body: postData, headers: headers);
  }

  Future getNotifications() async {
    return http.get(
      '$url/getNotifications',
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ' + await this.getTkn(),
      },
    );
  }

  Future<http.Response> getUserDetails() async {
    return http.get('$url/getUser',
        headers: {'Authorization': 'Bearer ' + await this.getTkn()});
  }

  Future<http.Response> getUserProfileDetails() async {
    return http.get('$url/getProfile',
        headers: {'Authorization': 'Bearer ' + await this.getTkn()});
  }

  Future<http.Response> getUserDashboard() async {
    return http.get('$url/getDashboard',
        headers: {'Authorization': 'Bearer ' + await this.getTkn()});
  }

  Future<http.Response> getUserCalendarList(data) async {
    var parameters;
    if (data['day'] != null)
      parameters = "/${data['year']}/${data['month']}/${data['day']}";
    else
      parameters = "/${data['year']}/${data['month']}";
    return http.get('$url/getCalendarList' + parameters,
        headers: {'Authorization': 'Bearer ' + await this.getTkn()});
  }

  Future<http.Response> getUserCalendar(data) async {
    var parameters = "/${data['year']}/${data['month']}";
    return http.get('$url/getCalendar' + parameters,
        headers: {'Authorization': 'Bearer ' + await this.getTkn()});
  }

  Future<http.Response> toggleNonBookableDate(data) async {
    var parameters;
    if (data['mode'] == 'multiple')
      parameters = "/${data['date']}/${data['time']}/multiple";
    // parameters = '/' + data['date'] + '/' + data['time'] + '/multiple';
    else
      parameters = "/${data['date']}/${data['time']}}";

    return http.get('$url/getCalendar' + parameters,
        headers: {'Authorization': 'Bearer ' + await this.getTkn()});
  }

  Future<http.Response> createDeviceToken(params) async {
    print('params-$params');
    return http.post('$url/createDeviceToken',
        headers: <String, String>{
          'Authorization': 'Bearer ' + await this.getTkn(),
        },
        body: params);
  }
}
