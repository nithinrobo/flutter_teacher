import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Bookings {
  String url = 'https://bankapi.nfamilyclub.com/v1';

  getTkn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('token');
  }

  //get booking lists
  Future<http.Response> getBookingList() async {
    return http.get('$url/admin/getBookingsAndTeachers', headers: {
      'Authorization': 'Bearer ' + await this.getTkn(),
    });
  }

  //get booking details by public id
  Future<http.Response> getBooking(data) async {
    return http.get('$url/getBooking/' + data['publicId'], headers: {
      'Authorization': 'Bearer ' + await this.getTkn(),
    });
  }

  //apply for request
  Future<http.Response> createRoleRequest(params) async {
    return http.post(
      '$url/createRoleRequest/' + params['roleId'].toString(),
      body: params,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: 'Bearer ' + await this.getTkn()
      },
    );
  }

//
  Future<http.Response> cancelRequest(params) async {
    return http.get(
      '$url/cancelBookingRequest/' + params['requestId'],
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ' + await this.getTkn(),
      },
    );
  }

  Future<http.Response> getSchool(data) async {
    return http.get('$url/getSchool/' + data['publicId'],
        headers: {'Authorization': 'Bearer ' + await this.getTkn()});
  }

  Future<http.Response> getAvailablity() async {
    return http.get('$url/getSingleAvailability',
        headers: {'Authorization': 'Bearer ' + await this.getTkn()});
  }

  Future<http.Response> createAvailablity(data) async {
    return http.post('$url/createAvailability',
        body: data,
        headers: {'Authorization': 'Bearer ' + await this.getTkn()});
  }

  Future<http.Response> deleteAvailablity(data) async {
    var _userId = data['id'];
    return http.get('$url/deleteAvailability/$_userId',
        headers: {'Authorization': 'Bearer ' + await this.getTkn()});
  }
  // createAvailability
}
