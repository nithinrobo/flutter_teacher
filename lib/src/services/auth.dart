import 'package:http/http.dart' as http;

class Auth {
  String url = 'https://bankapi.nfamilyclub.com/v1';

  //login api
  Future<http.Response> login(params) async {
    var apiUrl = '$url/login';
    return http.post(apiUrl, body: params);
  }

  Future<http.Response> forgotPassword(params) async {
    var apiUrl = '$url/requestPasswordReset';
    return http.post(apiUrl, body: params);
  }

//  requestPasswordReset
}
