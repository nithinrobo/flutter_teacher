import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class TimeSheetService {
  String url = 'https://bankapi.nfamilyclub.com/v1';

  getTkn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('token');
  }

  Future<http.Response> getTimeSheets() async {
    return http.get('$url/getTimesheets', headers: {
      'Authorization': 'Bearer ' + await this.getTkn(),
    });
  }

//  getTimesheet
  Future<http.Response> getTimeSheet(hash) async {
    var _id = hash;
    return http.get('$url/getTimesheet/$_id', headers: {
      'Authorization': 'Bearer ' + await this.getTkn(),
    });
  }

  Future<http.Response> confirmTimeSheet(hash) async {
    var _id = hash;
    return http.get('$url/confirmTimesheet/$_id', headers: {
      'Authorization': 'Bearer ' + await this.getTkn(),
    });
  }

  Future<http.Response> contestTimeSheet(hash) async {
    var _id = hash;
    return http.get('$url/contestTimesheet/$_id', headers: {
      'Authorization': 'Bearer ' + await this.getTkn(),
    });
  }

  // contestTimesheet
}

//confirmTimesheet
