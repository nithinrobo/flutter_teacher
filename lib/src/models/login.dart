class LoginModel {
  String token = '';
  List roles = [];
  String firstName = '';
  String lastName = '';
  int id = 0;
  String err = '';

  LoginModel({token, roles, firstName, lastName, id});

  LoginModel.fromJson(Map<String, dynamic> parsedJson) {
    token = parsedJson['token'];
    roles = parsedJson['roles'];
    firstName = parsedJson['user']['firstName'];
    lastName = parsedJson['user']['lastName'];
    id = parsedJson['user']['id'];
    err = parsedJson['error'];
  }
}
